<?php

require '../vendor/autoload.php';

use Philo\Blade\Blade;
use Clases\Producto;

$views = '../views';
$cache = '../cache';

$blade = new Blade($views, $cache);
$titulo='Productos';
$encabezado='Listado de Productos';
$productos= 'productos';
echo $blade->view()->make('vistaProductos', compact('titulo', 'encabezado', 'productos'))->render();
?>