<?php

require '../vendor/autoload.php';

use Philo\Blade\Blade;
use Clases\Familia;

$views = '../views';
$cache = '../cache';

$blade = new Blade($views, $cache);
$titulo='Familias';
$encabezado='Listado de Familias';
$familias=(new Familia())->recuperarFamilias();
echo $blade->view()->make('vistaFamilias', compact('titulo', 'encabezado', 'familias'))->render();
?>