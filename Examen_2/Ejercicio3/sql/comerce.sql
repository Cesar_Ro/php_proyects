CREATE DATABASE IF NOT EXISTS `Inmobiliaria`;
use `Inmobiliaria`;

CREATE TABLE IF NOT EXISTS 'productos' (
    'codigo_producto' CHAR(5) NOT NULL,
    'nombre' VARCHAR(100) NOT NULL,
    'descripcion'  VARCHAR(1000),
    'precio'  DEC(6,2) NOT NULL,
    PRIMARY KEY(codigo_producto)
    );

INSERT INTO productos (codigo_producto, nombre, descripcion, precio) VALUES
        ("00001", "Camiseta de Zayas", "Esta camiseta es de 100% algodon y nos representa a nuestro Instituto", 17.95),
        ("00002", "Pegatina para parachoques ", "Con mucho color , esta pegatina nos permite identificarnos como alumnos del IES Maria de Zayas", 5.95),
        ("00003", "Taza de Cafe", "Con nuestro logo podremos beber un cafe cada mañana en esta taza de buena porcelana. Se puede meter al lavaplatos y microhondas.", 8.95),
        ("00004", "Disfraz de Superheroe", "Tenemos una selección completa de colores y tamaños para que usted elija . Este traje es elegante, con estilo , y muestra las  habilidades de lucha contra el crimen o habilidades intrigantes mal.", 99.95),
        ("00005", "Gancho pequeño", "Este gancho especializado te sacará de los lugares más estrechos. Especialmente diseñado para la portabilidad y el sigilo .Tenga en cuenta Que este gancho viene con un límite de peso", 139.95),
        ("00006", "Gancho grande", "Gran versión de nuestro gancho de ataque y será seguro de transportar", 199.95);

CREATE TABLE IF NOT EXISTS 'clientes' (
    'cod_cliente' INTEGER NOT NULL AUTO_INCREMENT,
    'nombre' VARCHAR(20) NOT NULL,
    'direccion' VARCHAR(50),
    'email' VARCHAR(100) NOT NULL,
    PRIMARY KEY (cod_cliente)
);

INSERT INTO clientes VALUES (7369,'SMITH',,'smith11@gmail.com');
INSERT INTO clientes VALUES (7499,'ALLEN','Romero 2','allen11@gmail.com');
INSERT INTO clientes VALUES (7521,'WARD','Velazquez 18','ward11@gmail.com');
INSERT INTO clientes VALUES (7566,'JONES',,'jones11@gmail.com');
INSERT INTO clientes VALUES (7654,'MARTIN','Gran Via 25','martin11@gmail.com');
INSERT INTO clientes VALUES (7698,'BLAKE',,'blake11@gmail.com');
INSERT INTO clientes VALUES (7782,'CLARK','Serrano 67','clark11@gmail.com');
INSERT INTO clientes VALUES (7788,'SCOTT',,'scott11@gmail.com');
INSERT INTO clientes VALUES (7839,'KING',,'king11@gmail.com');
INSERT INTO clientes VALUES (7844,'TURNER',,'turner11@gmail.com');
INSERT INTO clientes VALUES (7876,'ADAMS','Infante Don Luis','adams11@gmail.com');
INSERT INTO clientes VALUES (7900,'JAMES',,'james11@gmail.com');
INSERT INTO clientes VALUES (7902,'FORD','Ortega y Gasset 67','ford11@gmail.com');
INSERT INTO clientes VALUES (7934,'MILLER',,'miller11@gmail.com');

CREATE TABLE IF NOT EXISTS detalles_compra (
    'cod_compra' INTEGER NOT NULL,
    'cantidad_producto' INTEGER NOT NULL,
    'cod_producto' CHAR(5) NOT NULL,
    'cod_cliente' INTEGER NOT NULL,
    FOREIGN KEY (cod_compra) REFERENCES compra(cod_compra),
    FOREIGN KEY (cod_producto) REFERENCES productos(codigo_producto)
);	





   

?>