<?php $__env->startSection('titulo'); ?>
    <?php echo e($titulo); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('paso'); ?>
    <?php echo e($paso); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contenido'); ?>
    <?php
        require '../vendor/autoload.php';
        use Examen\Viviendas;

        session_start();
        if (isset($_SESSION['tipo'])) {

        if ($_SESSION['precio'] == 4) $precio = "precio>=300000";
        else {
            $min = ($_SESSION['precio'] - 1) * 100000;
            $max = $_SESSION['precio'] * 100000;
            $precio = "precio>=$min AND precio<$max";
        }
        $arr = "tipo='{$_SESSION['tipo']}' AND zona='{$_SESSION['zona']}' AND dormitorios={$_SESSION['dormitorios']} AND {$precio} AND garage='{$_POST['garage']}' AND
         jardin='{$_POST['jardin']}' AND padel='{$_POST['padel']}' AND piscina='{$_POST['piscina']}' AND zonascomunes='{$_POST['zonascomunes']}'";
        $result = Viviendas::getViviendas($arr);
        if (!$result) echo "No se encuentran resultados";
        else {
            echo "<p><strong>Viviendas encontradas:</strong></p>
            <table>
            <td>Tipo</td>
            <td>Zona</td>
            <td>Dormitorios</td>
            <td>Precio</td>
            <td>Garage</td>
            <td>Jardin</td>
            <td>Padel</td>
            <td>Piscina</td>
            <td>Zona comunes</td>";

            foreach ($result as $vivienda) {
                $vivienda->printRow();
            }
            echo "</table>";
        }
    }
    else echo "Acceso incorrecto";
    ?>
    <br>
    <a href="controller.php">Nueva Busqueda</a>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('plantillas.plantillaFormulario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\demo\php_proyects\Examen_2\Ejercicio2\views/vistaResults.blade.php ENDPATH**/ ?>