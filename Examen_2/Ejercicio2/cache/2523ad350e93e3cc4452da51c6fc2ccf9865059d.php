<?php $__env->startSection('titulo'); ?>
    <?php echo e($titulo); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('paso'); ?>
    <?php echo e($paso); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('encabezado'); ?>
    <?php echo e($encabezado); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contenido'); ?>
<fieldset>
    <form action="./results.php" method="post">
        <label for="garage"><strong>Garage:  </strong></label>
        <select name="garage">
            <option>No</option>
            <option>Si</option>
        </select>

        <label for="jardin"><strong>Jardin:  </strong></label>
        <select name="jardin">
            <option>No</option>
            <option>Si</option>
        </select>

        <label for="padel"><strong>Padel:  </strong></label>
        <select name="padel">
            <option>No</option>
            <option>Si</option>
        </select>

        <label for="piscina"><strong>Piscina:  </strong></label>
        <select name="piscina">
            <option>No</option>
            <option>Si</option>
        </select>
        
        <label for="zonascomunes"><strong>Zonas comunes:  </strong></label>
        <select name="zonascomunes">
            <option>No</option>
            <option>Si</option>
        </select>
        <?php
            session_start();
            $_SESSION['dormitorios'] = $_POST['dormitorios'];
            $_SESSION['precio'] = $_POST['precio'];
        ?>
        <br>
        <a href='./caracteristicas.php'>Anterior</a>
        <input type="submit" value="Siguiente" />
    </form>
</fieldset>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('plantillas.plantillaFormulario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\demo\php_proyects\Examen_2\Ejercicio2\views/vistaExtras.blade.php ENDPATH**/ ?>