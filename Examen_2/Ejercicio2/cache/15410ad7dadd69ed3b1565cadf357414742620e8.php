<?php $__env->startSection('titulo'); ?>
    <?php echo e($titulo); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('paso'); ?>
    <?php echo e($paso); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('encabezado'); ?>
    <?php echo e($encabezado); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contenido'); ?>
<fieldset>
    <form action="./caracteristicas.php" method="post">
        <label for="zona"><strong>Zona:  </strong></label>
        <select name="zona">
        <?php
            require '../vendor/autoload.php';
            use Examen\DBconnection;

            session_start();

            if (isset($_POST['tipo'])) $_SESSION['tipo'] = $_POST['tipo'];

            $connection = new DBconnection();
            $stm = $connection->getConnect()->query("SELECT DISTINCT zona FROM zonas");
            $answer = $stm->fetchAll();

            foreach ($answer as $zona) {
                echo "<option>{$zona[0]}</option>";
            }
            echo "</select>";
        ?>
        <br>
        <a href='./tipo.php'>Anterior</a>
        <input type="submit" value="Siguiente" />
    </form>
        </fieldset>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('plantillas.plantillaFormulario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\demo\php_proyects\Examen_2\Ejercicio2\views/vistaZona.blade.php ENDPATH**/ ?>