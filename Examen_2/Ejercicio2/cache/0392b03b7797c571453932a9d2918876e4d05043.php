<?php $__env->startSection('titulo'); ?>
    <?php echo e($titulo); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('paso'); ?>
    <?php echo e($paso); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('encabezado'); ?>
    <?php echo e($encabezado); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contenido'); ?>
<fieldset>
    <form action="./zona.php" method="post">  
        <label for="tipo"><strong>Tipo:  </strong></label>
        <select name="tipo">
        <?php
            require '../vendor/autoload.php';
            use Examen\DBconnection;

            session_start();

            $connection = new DBconnection();
            $stm = $connection->getConnect()->query("SELECT DISTINCT tipo FROM tipos");
            $answer = $stm->fetchAll();
                
            foreach ($answer as $tipo) {
                echo "<option>{$tipo[0]}</option>";
            }
        ?>
        </select>
        <br><input type="submit" value="Siguiente" />
    </form>
        </fieldset>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('plantillas.plantillaFormulario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\demo\php_proyects\Examen_2\Ejercicio2\views/vistaTipo.blade.php ENDPATH**/ ?>