<?php $__env->startSection('titulo'); ?>
    <?php echo e($titulo); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('paso'); ?>
    <?php echo e($paso); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('encabezado'); ?>
    <?php echo e($encabezado); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contenido'); ?>
<fieldset>
    <form action="./extras.php" method="post">
        <label for="dormitorios"><strong>Dormitorios:  </strong></label>
            <input type="radio" name="dormitorios" value="1" checked>1</input>
            <input type="radio" name="dormitorios" value="2">2</input>
            <input type="radio" name="dormitorios" value="3">3</input>
            <input type="radio" name="dormitorios" value="4">4</input>
            <input type="radio" name="dormitorios" value="5">5</input>
        <br>
        <label for="precio"><strong>Precio (€):  </strong></label>
            <input type="radio" name="precio" value="1" checked><100000</input>
            <input type="radio" name="precio" value="2">100000 - 200000</input>
            <input type="radio" name="precio" value="3">200000 - 300000</input>
            <input type="radio" name="precio" value="4">> 300000</input>
        <?php
            session_start();
            if (isset($_POST['zona'])) $_SESSION['zona'] = $_POST['zona'];
        ?>
        <br>
        <a href='./zona.php'>Anterior</a>
        <input type="submit" value="Siguiente" />
    </form>
</fieldset>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('plantillas.plantillaFormulario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\demo\php_proyects\Examen_2\Ejercicio2\views/vistaCaracteristicas.blade.php ENDPATH**/ ?>