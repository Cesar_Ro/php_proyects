<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $__env->yieldContent('titulo'); ?></title>
</head>
<body>
    <h1>Busqueda de Vivienda</h1>
    <p>Busqueda - <?php echo $__env->yieldContent('paso'); ?></p>
    <h1><?php echo $__env->yieldContent('encabezado'); ?></h1>
        <?php echo $__env->yieldContent('contenido'); ?>
</body>
</html><?php /**PATH C:\wamp64\www\demo\php_proyects\Examen_2\Ejercicio2\views/plantillas/plantillaFormulario.blade.php ENDPATH**/ ?>