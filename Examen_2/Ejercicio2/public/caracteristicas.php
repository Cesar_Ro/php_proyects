<?php
require '../vendor/autoload.php';
use Philo\Blade\Blade;

$views = '../views';
$cache = '../cache';

$titulo = 'Caracteristicas';
$paso = "1.Tipo > 2.Zona > <strong>3.Caracteristicas</strong> > 4.Extras";
$encabezado = 'Paso 3: Elija las caracteristicas de la vivienda';

$blade = new Blade($views, $cache);

echo $blade->view()->make('vistaCaracteristicas', compact('titulo', 'paso', 'encabezado'))->render();
?>