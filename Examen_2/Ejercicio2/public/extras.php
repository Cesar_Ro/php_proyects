<?php
require '../vendor/autoload.php';
use Philo\Blade\Blade;

$views = '../views';
$cache = '../cache';

$titulo = 'Extras';
$paso = "1.Tipo > 2.Zona > 3.Caracteristicas > <strong>4.Extras</strong>";
$encabezado = 'Paso 4: Elija los extras de la vivienda';

$blade = new Blade($views, $cache);

echo $blade->view()->make('vistaExtras', compact('titulo', 'paso', 'encabezado'))->render();
?>