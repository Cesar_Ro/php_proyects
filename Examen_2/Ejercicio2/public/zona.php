<?php
require '../vendor/autoload.php';
use Philo\Blade\Blade;

$views = '../views';
$cache = '../cache';

$titulo = 'Zona';
$paso = "1.Tipo > <strong>2.Zona</strong> > 3.Caracteristicas > 4.Extras";
$encabezado = 'Paso 2: Elija la zona de la vivienda';

$blade = new Blade($views, $cache);

echo $blade->view()->make('vistaZona', compact('titulo', 'paso', 'encabezado'))->render();
?>