<?php
require '../vendor/autoload.php';
use Philo\Blade\Blade;
use Examen\Viviendas;

$views = '../views';
$cache = '../cache';

$titulo = 'Resultados';
$paso = 'Resultados de busqueda';

$blade = new Blade($views, $cache);

echo $blade->view()->make('vistaResults', compact('titulo', 'paso'))->render();
?>