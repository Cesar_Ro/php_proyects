<?php
require '../vendor/autoload.php';
use Philo\Blade\Blade;

$views = '../views';
$cache = '../cache';

$titulo = 'Tipo';
$paso = "<strong>1.Tipo</strong> > 2.Zona > 3.Caracteristicas > 4.Extras";
$encabezado = 'Paso 1: Elija el tipo de vivienda';

$blade = new Blade($views, $cache);

echo $blade->view()->make('vistaTipo', compact('titulo', 'paso', 'encabezado'))->render();
?>