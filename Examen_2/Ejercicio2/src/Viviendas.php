<?php

namespace Examen;

class Viviendas extends DBconnection {
    private $tipo;
    private $zona;
    private $dormitorios;
    private $precio;
    private $garage;
    private $jardin;
    private $padel;
    private $piscina;
    private $zonascomunes;

    public function __construct($array) {
        $this->tipo = $array[0];
        $this->zona = $array[1];
        $this->dormitorios = $array[2];
        $this->precio = $array[3];
        $this->garage = $array[4];
        $this->jardin = $array[5];
        $this->padel = $array[6];
        $this->piscina = $array[7];
        $this->zonascomunes = $array[8];
        parent::__construct();
    }

// DML Public

    public function insert() {
        $insert = $this->connect->prepare("INSERT INTO viviendas (tipo, zona, dormitorios, precio, garage, jardin, padel, piscina, zonascomunes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $insert->execute([$this->tipo, $this->zona, $this->dormitorios, $this->precio, $this->garage, $this->jardin, $this->padel, $this->piscina, $this->zonascomunes]);
        return $insert->errorInfo();
    }

    public function printRow() {
        echo "<tr>
            <td>{$this->tipo}</td>
            <td>{$this->zona}</td>
            <td>{$this->dormitorios}</td>
            <td>{$this->precio}</td>
            <td>{$this->garage}</td>
            <td>{$this->jardin}</td>
            <td>{$this->padel}</td>
            <td>{$this->piscina}</td>
            <td>{$this->zonascomunes}</td>
            </tr>";
    }

// DML Static 

    public static function getViviendas($arr) {
        $return = [];
        $connection = new DBconnection();
        $stm = $connection->getConnect()->query("SELECT tipo, zona, dormitorios, precio, garage, jardin, padel, piscina, zonascomunes FROM viviendas WHERE $arr");
        $answer = $stm->fetchAll();
        foreach ($answer as $row) {
            $vivienda = new Viviendas($row);
            array_push($return, $vivienda);
        }
        return $return;
    }

    public static function getViviendasByID($id) {
        return Viviendas::getViviendas("id=$id");
    }

    public static function insertXml($xmlFile) {
        foreach($xmlFile as $vivienda) {
            $array = [
                $vivienda->tipo,
                $vivienda->zona,
                $vivienda->dormitorios,
                $vivienda->precio,
                $vivienda->extras->garage,
                $vivienda->extras->jardin,
                $vivienda->extras->padel,
                $vivienda->extras->piscina,
                $vivienda->extras->zonascomunes
            ];
            $newInsert = new Viviendas($array);
            $result = $newInsert->insert();
        }
    }
}
?>