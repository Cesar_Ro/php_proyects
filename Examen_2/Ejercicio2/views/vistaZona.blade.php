@extends('plantillas.plantillaFormulario')
@section('titulo')
    {{$titulo}}
@endsection
@section('paso')
    {{$paso}}
@endsection
@section('encabezado')
    {{$encabezado}}
@endsection
@section('contenido')
<fieldset>
    <form action="./caracteristicas.php" method="post">
        <label for="zona"><strong>Zona:  </strong></label>
        <select name="zona">
        <?php
            require '../vendor/autoload.php';
            use Examen\DBconnection;

            session_start();

            if (isset($_POST['tipo'])) $_SESSION['tipo'] = $_POST['tipo'];

            $connection = new DBconnection();
            $stm = $connection->getConnect()->query("SELECT DISTINCT zona FROM zonas");
            $answer = $stm->fetchAll();

            foreach ($answer as $zona) {
                echo "<option>{$zona[0]}</option>";
            }
            echo "</select>";
        ?>
        <br>
        <a href='./tipo.php'>Anterior</a>
        <input type="submit" value="Siguiente" />
    </form>
        </fieldset>
@endsection