<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('titulo')</title>
</head>
<body>
    <h1>Busqueda de Vivienda</h1>
    <p>Busqueda - @yield('paso')</p>
    <h1>@yield('encabezado')</h1>
        @yield('contenido')
</body>
</html>