@extends('plantillas.plantillaFormulario')
@section('titulo')
    {{$titulo}}
@endsection
@section('paso')
    {{$paso}}
@endsection
@section('encabezado')
    {{$encabezado}}
@endsection
@section('contenido')
<fieldset>
    <form action="./results.php" method="post">
        <label for="garage"><strong>Garage:  </strong></label>
        <select name="garage">
            <option>No</option>
            <option>Si</option>
        </select>

        <label for="jardin"><strong>Jardin:  </strong></label>
        <select name="jardin">
            <option>No</option>
            <option>Si</option>
        </select>

        <label for="padel"><strong>Padel:  </strong></label>
        <select name="padel">
            <option>No</option>
            <option>Si</option>
        </select>

        <label for="piscina"><strong>Piscina:  </strong></label>
        <select name="piscina">
            <option>No</option>
            <option>Si</option>
        </select>
        
        <label for="zonascomunes"><strong>Zonas comunes:  </strong></label>
        <select name="zonascomunes">
            <option>No</option>
            <option>Si</option>
        </select>
        <?php
            session_start();
            $_SESSION['dormitorios'] = $_POST['dormitorios'];
            $_SESSION['precio'] = $_POST['precio'];
        ?>
        <br>
        <a href='./caracteristicas.php'>Anterior</a>
        <input type="submit" value="Siguiente" />
    </form>
</fieldset>
@endsection

