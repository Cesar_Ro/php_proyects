@extends('plantillas.plantillaFormulario')
@section('titulo')
    {{$titulo}}
@endsection
@section('paso')
    {{$paso}}
@endsection
@section('encabezado')
    {{$encabezado}}
@endsection
@section('contenido')
<fieldset>
    <form action="./zona.php" method="post">  
        <label for="tipo"><strong>Tipo:  </strong></label>
        <select name="tipo">
        <?php
            require '../vendor/autoload.php';
            use Examen\DBconnection;

            session_start();

            $connection = new DBconnection();
            $stm = $connection->getConnect()->query("SELECT DISTINCT tipo FROM tipos");
            $answer = $stm->fetchAll();
                
            foreach ($answer as $tipo) {
                echo "<option>{$tipo[0]}</option>";
            }
        ?>
        </select>
        <br><input type="submit" value="Siguiente" />
    </form>
        </fieldset>
@endsection