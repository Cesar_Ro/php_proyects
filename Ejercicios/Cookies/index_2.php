<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <fieldset>
        <legend>Configuración</legend>
        <?php

        //Comprobar si viene de ver_formulario.php para eliminar Cookies
        if (isset($_GET['delete'])) {
            setcookie('color');
            setcookie('idioma');
            setcookie('fuente');
            header('Refresh: 0');
        }

        //Comprobar si hay Cookies de preferencias
        if (isset($_COOKIE['color']) && isset($_COOKIE['idioma']) && isset($_COOKIE['fuente'])) header('Location: ver_formulario_2.php');

        //Comprobar si tenemos valores Post
        else if (!isset($_POST['color']) || !isset($_POST['idioma']) || !isset($_POST['fuente'])) {

        $url = $_SERVER['PHP_SELF'];

        echo<<<EOT
        <form action="$url" method="post">
            <label for="color">Color de fondo</label><br>
            <input type="color" name="color" value="#ffffff">
            <br>
            <label for="fuente">Color de fuente</label><br>
            <input type="color" name="fuente">
            <br>
            <label for="idioma">Idioma</label><br>
            <input type="radio" name="idioma" value="español" checked>Español
            <input type="radio" name="idioma" value="ingles">Inglés
            <input type="radio" name="idioma" value="frances">Francés
            <input type="radio" name="idioma" value="italiano">Italiano
            <input type="radio" name="idioma" value="aleman">Aleman
            <br>
            <input type="submit" value="Confirmar">
        </form>
        EOT;
        }

        //Crear Cookies
        else {
            setcookie('color', $_POST['color'], time()+60*60*24*30);
            setcookie('idioma', $_POST['idioma'], time()+60*60*24*30);
            setcookie('fuente', $_POST['fuente'], time()+60*60*24*30);
            header('Location: ver_formulario_2.php');
        }
        ?>
    </fieldset>
    
</body>
</html>