<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <?php
   //Comprobar si hay Cookies, si no hay ir a index.php para seleccionar preferencias
   if (!isset($_COOKIE['color']) && !isset($_COOKIE['idioma']) && !isset($_COOKIE['fuente'])) header('Location: index_2.php');

   $color = $_COOKIE['color'];
   $idioma = $_COOKIE['idioma'];
   $fuente = $_COOKIE['fuente'];

   $lang = json_decode(file_get_contents('palabras.json'), true);

   $user = $lang[$idioma]['user'];
   $pass = $lang[$idioma]['pass'];

   echo<<<EOT
   <style>
    fieldset {
      background-color: $color;
    }
    body {
      color:$fuente;
    }
   </style>
   EOT;
  ?>
 </head>
 <body>
  <fieldset>
   <?php
    echo<<<EOT
     <form>
      <label for="user">$user</label>
      <input type="text" name="user">
      <label for="pass">$pass</label>
      <input type="text" name="pass">
      <br>
      <input type="submit" value="Login">
    EOT;
   ?>
   </form>
   <a href="./index_2.php?delete=1"><button>Preferencias</button></a>
  </fieldset>
 </body>
</html>