<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="./style.css">
  <title>Document</title>
 </head>
 <body>
  <?php

   function print_month($year, $month) {
    $day = 1;
    $date = mktime(0, 0, 0, $month, $day, $year);
    $weekday = date("N", $date);
    $month_name = date("F", $date);

    echo<<<EOT
<table>
 <tr>
  <td class="name" colspan="7">$month_name</td>
  </tr>
  <tr>
   <td class="subname">L</td>
   <td class="subname">M</td>
   <td class="subname">X</td>
   <td class="subname">J</td>
   <td class="subname">V</td>
   <td class="subname">S</td>
   <td class="subname">D</td>
  </tr>
  <tr>
EOT;

    for ($i = 1; $i < $weekday; $i++) {
     echo "<td> </td>";
    }

    while (checkdate($month, $day, $year)) {
     echo "<td>$day</td>";
     $day++;
     $weekday++;
     if($weekday > 7) {
      echo '</tr><tr>';
      $weekday = 1;
     }
    }
    for ($weekday; $weekday <= 7; $weekday++) {
     echo "<td> </td>";
    }
    echo '</tr></table>';
   }

   function print_year($year) {
    echo "<table><tr><td class=\"name\" colspan=\"4\">$year</td></tr><tr>";
    for ($month = 1; $month <= 12; $month++) {
     echo '<td>';
     print_month($year, $month);
     if ($month%4 == 0) echo "</tr><tr>";
     echo '</td>';
    }
    echo "</tr></table>";
   }


   if(isset($_POST['year']) && !empty($_POST['year'])) {
    if(isset($_POST['month']) && !empty($_POST['month'])) {
     print_month($_POST['year'], $_POST['month']);
    }
    else print_year($_POST['year']); 
   }

   else {
    echo<<<EOT
<fieldset>
 <form action="./calendario.php" method="post">
  <input type="number" name="year" value="Año">
  <select name="month">
   <option></option>
   <option value="1">Enero</option>
   <option value="2">Febrero</option>
   <option value="3">Marzo</option>
   <option value="4">Abril</option>
   <option value="5">Mayo</option>
   <option value="6">Junio</option>
   <option value="7">Julio</option>
   <option value="8">Agosto</option>
   <option value="9">Septiembre</option>
   <option value="10">Octubre</option>
   <option value="11">Noviembre</option>
   <option value="12">Diciembre</option>
  </select>
  <input type="submit">
 </form>
</fieldset>
EOT;
echo date("");
   }
  ?>   
 </body>
</html>