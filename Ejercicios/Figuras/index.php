<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <title>Document</title>
</head>
<body>
    <?php
    require '../vendor/autoload.php';
    use Figuras\CCuadrilatero;
    use Figuras\CCirculo;
    use Figuras\CTriangulo;
    use Figuras\CPentagono;
    use Figuras\CHexagono;
    use Clases\CHeader;

    $header = new CHeader('Figuras', '#3A70A6', '#EBF5F5');
    echo $header;

    if(!isset($_POST['btn-select']) || empty($_POST['figura'])) {
    echo <<<EOT
    <section>
     <form action="." method="post">
      <div class="select">
       <input type="radio" name="figura" value="1"/>Circulo
       <input type="radio" name="figura" value="2"/>Triangulo
       <input type="radio" name="figura" value="3"/>Cuadrilatero
       <input type="radio" name="figura" value="4"/>Pentagono
       <input type="radio" name="figura" value="5"/>Hexagono
      </div>
      <div class="enviar">
       <input type="submit" name="btn-select" value="Seleccionar">
      </div>
     </form>
    </section>
    EOT;
    }

    else {
        switch($_POST['figura']) {
            case 1:
                echo CCirculo::inputForm();
                break;
            case 2:
                echo CTriangulo::inputForm();
                break;
            case 3:
                echo CCuadrilatero::inputForm();
                break;
            case 4:
                echo CPentagono::inputForm();
                break;
            case 5:
                echo CHexagono::inputForm();
                break;
            default:
                header('Location: error.php');
        }
    }
    ?>
</body>
</html>