<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
    <title>Document</title>
</head>
<body>
    <?php
    require '../vendor/autoload.php';
    use Figuras\CCuadrilatero;
    use Figuras\CCirculo;
    use Figuras\CTriangulo;
    use Figuras\CPentagono;
    use Figuras\CHexagono;
    use Clases\CHeader;

    if(!isset($_POST['btn-data']) || !empty($_POST['fig'])) {
        $name = $_POST['fig'];
        switch($name) {
            case "Circulo": 
                $figura = new CCirculo($_POST['radio']);
                break;
            case "Triangulo":
                $figura = new CTriangulo($_POST['side']);
                break;
            case "Cuadrilatero":
                $figura = new CCuadrilatero($_POST['sideA'], $_POST['sideB']);
                break;
            case "Pentagono":
                $figura = new CPentagono($_POST['side']);
                break;
            case "Hexagono":
                $figura = new CHexagono($_POST['side']);
                break;
            default:
                header('Location: error.php');
        }
    }

    else header('Location: error.php');

    $header = new CHeader($name, '#3A70A6', '#EBF5F5');
    echo $header;

    echo '<section>';
    $figura->showImage();
    echo $figura;
    echo '<button onclick="document.location=\'index.php\'">Nueva figura</button>';
    echo '</section>';  
    ?>
</body>
</html>