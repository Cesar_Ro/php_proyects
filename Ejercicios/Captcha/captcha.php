<?php
//Dimensiones
$ancho = 100;
$alto = 50;

//Colores
$imagen = imagecreatetruecolor($ancho, $alto);
$negro = imagecolorallocate($imagen, 0, 0, 0);
$gris = imagecolorallocate($imagen, 100, 100, 100);
$rgb[0] = rand(0,255);
$rgb[1] = rand(0,255);
$rgb[2] = rand(0,255);
$colorAleatorio = imagecolorallocate($imagen, $rgb[0], $rgb[1], $rgb[2]);
$colorAleatorioInvertido = imagecolorallocate($imagen, 255-$rgb[0], 255-$rgb[1], 255-$rgb[2]);

//Fondo
imagefill($imagen, 0, 0, $colorAleatorio);

//Marco
imagerectangle($imagen, 0, 0, $ancho, $alto, $negro);

//Rejilla
imageline($imagen, 25, 0, 25, $alto, $gris);
imageline($imagen, 50, 0, 50, $alto, $gris);
imageline($imagen, 75, 0, 75, $alto, $gris);
imageline($imagen, 0, 13, $ancho, 13, $gris);
imageline($imagen, 0, 26, $ancho, 26, $gris);
imageline($imagen, 0, 39, $ancho, 39, $gris);

//Escribir horizontalmente
$random = substr(str_replace("0", "", str_replace("O", "", strtoupper(md5(rand(9999, 99999))))), 0, 5);
imagestring($imagen, 6, 26, 18, $random, $colorAleatorioInvertido);

//Escribir verticalmente
$random = substr(str_replace("5", "", str_replace("O", "", strtoupper(md5(rand(9999, 99999))))), 0, 5);
imagestringup($imagen, 6, 0, 45, $random, $colorAleatorioInvertido);

//Añadir ruido para que sea más ilegible
for ($i = 0;$i <= 777; $i++) {
    $randx = rand(0,100);
    $randy = rand(0,50);
    imagesetpixel($imagen, $randx, $randy, $colorAleatorioInvertido);
};

//Salida
header("Content-type: image/png");
imagepng($imagen);
imagedestroy($imagen);
?>