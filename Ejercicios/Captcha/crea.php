<?php
    $imagen = imagecreatetruecolor(250, 250);

    //Colores
    $grey = imagecolorallocate($imagen, 200, 200, 200);
    $grey_dark = imagecolorallocate($imagen, 150, 150, 150);
    $red = imagecolorallocate($imagen, 255, 0, 0);
    $green = imagecolorallocate($imagen, 0, 255, 0);
    $black = imagecolorallocate($imagen, 0, 0, 0);
    $blue = imagecolorallocate($imagen, 0, 0, 255);
    $white = imagecolorallocate($imagen, 255, 255, 255);

    //Fondo
    imagefill($imagen, 50, 50, $grey);
    imagefill($imagen, 150, 150, $grey_dark);

    //Marcas
    imageline($imagen, 0, 50, 50, 0, $red); //linea
    imagesetpixel($imagen, 10, 10, $red); //punto

    //Ángulos
    imagefilledarc($imagen, 200, 200, 90, 90, 180, 270, $green, IMG_ARC_PIE); //quesito
    imagefilledarc($imagen, 200, 200, 90, 90, 270, 0, $blue, IMG_ARC_CHORD); //triangulo relleno
    imagefilledarc($imagen, 200, 200, 90, 90, 0, 90, $red, IMG_ARC_NOFILL); //borde
    imagefilledarc($imagen, 200, 200, 90, 90, 90, 180, $white, IMG_ARC_EDGED | IMG_ARC_NOFILL); //quesito borde

    //Rectángulo
    imagefilledrectangle($imagen, 50, 150, 100, 200, $green); //relleno
    imagerectangle($imagen, 50, 150, 100, 200, $black); //borde

    //Poligono
    $puntosArray = array(
        200, 50,
        225, 50,
        230, 75,
        220, 80,
        180, 60
    );
    $numeroPuntos = count($puntosArray)/2;

    imagefilledpolygon($imagen, $puntosArray, $numeroPuntos, $white); //relleno
    imagepolygon($imagen, $puntosArray, $numeroPuntos, $blue); //borde

    //Elipse
    imagefilledellipse($imagen, 125, 125, 75, 50, $green);

    //Texto
    $ttf = 'font\OpenSans-Regular.ttf';

    imagestring($imagen, 3, 10, 75, 'C', $black);
    imagestringup($imagen, 3, 12, 80, 'E', $black);
    //imagefttext($imagen, 12, 315, 20, 75, $red, $ttf, 'SAR'); //No funciona

    //Salida
    header("Content-type: image/png");
    imagepng($imagen);
    imagedestroy($imagen);  
?>