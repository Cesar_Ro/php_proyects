<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
    <title>Document</title>
</head>
<body>
  <?php
  include "./functions.php";

  if(isset($_POST['user']) && !empty($_POST['user'])) {
   $user = $_REQUEST['user'];
   $password = $_REQUEST['password'];

   (check_pass($user, $password)) ? header('Location: verdatos.php') : header("Refresh: 2; url=./error.php?user=$user");
  }

  else {

  echo<<<EOT
<form name="acreditacion" action="index.php" method="post">
 <label for="user">Usuario</label>
 <input type="text" name="user">
 <label for="password">Contraseña</label>
 <input type="text" name="password">
 <br>
 <input type="submit" value="Login">
</form> 
EOT; 
  }
  ?>
</body>
</html>