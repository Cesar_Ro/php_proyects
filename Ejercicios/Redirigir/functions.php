<?php
function check_pass($user, $pass) {
    $archivo = './datos.txt';
    $array_users = file($archivo, FILE_IGNORE_NEW_LINES);
    for ($i = 0; $i < count($array_users); $i++) {
        $list = explode('|',$array_users[$i]);
        $name = $list[0];
        $word = $list[1];
        if ($name == $user) {
            if ($word == $pass) return true;
            else return false;
        }
    }
    return false;
}

function find_user($user) {
    $archivo = './datos.txt';
    $array_users = file($archivo, FILE_IGNORE_NEW_LINES);
    for ($i = 0; $i < count($array_users); $i++) {
        $name = explode('|',$array_users[$i], 0);
        if ($name == $user) return true;
    }
    return false;
}

function new_user($user, $pass) {
    if (find_user($user)) return false;
    $archivo = './datos.txt';
    $entry = "\r\n".$user.'|'.$pass;
    file_put_contents($archivo, $entry, FILE_APPEND);
    return true;
}
?>