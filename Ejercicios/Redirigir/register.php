<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
    <title>Document</title>
</head>
<body>
  <?php
   include "./functions.php";

   if(isset($_POST['user']) && !empty($_POST['user'])) {
    if (new_user($_POST['user'], $_POST['password'])) {
        header ('refresh: 3; url=./index.php');
        echo '<p>Usuario registrado</p>';
    }
    else {
        header ('refresh: 3; url=./error.php');
        echo 'El usuario ya existe';
    }
   }

   else {
  echo<<<EOT
<p>Registro</p>
<form name="acreditacion" action="./register.php" method="post">
 <label for="user">Usuario</label>
 <input type="text" name="user">
 <label for="password">Contraseña</label>
 <input type="text" name="password">
 <br>
 <input type="submit" value="Register">
</form> 
EOT; 
   }
  ?>
</body>
</html>