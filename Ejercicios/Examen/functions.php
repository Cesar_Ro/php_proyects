<?php
    function crea_usuario($name, $pass, $user = 'default') {
        $usuarios = json_decode(file_get_contents("../datos/usuarios.json"));
        if ($user == 'default') {
            $max = 0;
            foreach ($usuarios as $usuario) {
                $num = substr($usuario[0], 3);
                if (is_int($num)) {
                    if ($num > $max) $max = $num;
                }
            }
            $max++;
            $user = 'DAW'.$max;
        }
        $new_user = [$user, $name, $pass];
        array_push($usuarios, $new_user);
        $result = file_put_contents("../datos/usuarios.json",json_encode($usuarios));
        if ($result === false) return false;
        else return true;
    }
?>