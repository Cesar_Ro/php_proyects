<?php
    function convertir($importe, $ratio) {
        return $importe * $ratio;
    }

    function pintar($importe, $origin, $change) {
        include "../datos/datos.php";
        global $url;
        
        //obtener ratio de conversión y convertir, actualizar documento contadores
        $ratio = $datos[$origin][$change];
        $resultado = convertir($importe, $ratio);
        update_contadores($origin, $change);

        //variables a imprimir: mayusculas y valor por unidad
        $text_origin = strtoupper($origin);
        $one_origin = $datos[$origin][$change];

        $text_change = strtoupper($change);
        $one_change = $datos[$change][$origin];

        echo<<<EOT
<h3>$importe $origin a $change = $resultado $change</h3>
<fieldset>
    <form action="$url" method="get">
        <label for="input">Importe</label>
        <input type="number" name="input" value="$importe">
        <label for="origin">De:</label>
        <select name="origin">
EOT;
        for ($i = 0; $i < count($divisas); $i++) {
            if ($divisas[$i] == $origin) echo "<option value=\"$divisas[$i]\" selected>$divisas[$i]</option>";
            else echo "<option value=\"$divisas[$i]\">$divisas[$i]</option>";
        }
        echo<<<'EOT'
        </select>
        <label for="change">Para:</label>
        <select name="change">
EOT;
        for ($i = 0; $i < count($divisas); $i++) {
            if ($divisas[$i] == $change) echo "<option value=\"$divisas[$i]\" selected>$divisas[$i]</option>";
            else echo "<option value=\"$divisas[$i]\">$divisas[$i]</option>";
        }
        echo<<<EOT
        </select>
        <input type="submit" name="refresh" value="Refresh">
    </form>
    <p>$importe $origin =</p>
    <h1>$resultado $change</h1>
    <p>1 $change = $one_change $origin</p>
    <p>1 $origin = $one_origin $change</p>
</fieldset>
EOT;
    }

    function update_total() {
        $contadores = json_decode(file_get_contents("../datos/contadores.json"), true);

        foreach ($contadores as $moneda) {
            $moneda['total'] = $moneda['from'] + $moneda['to'];
        }
        file_put_contents("../datos/contadores.json", json_encode($contadores));
    }

    function update_percent() {
        $contadores = json_decode(file_get_contents("../datos/contadores.json"), true);
        $total = 0;

        foreach ($contadores as $moneda) {
            $total += $moneda['total'];
        }

        foreach ($contadores as $moneda) {
            $moneda['percentage'] = $moneda['from']/$total;
        }
        file_put_contents("../datos/contadores.json", json_encode($contadores));
    }

    function update_uses($from, $to) {
        $contadores = json_decode(file_get_contents("../datos/contadores.json"), true);

        $contadores[$from]["from"] += 1;
        $contadores[$to]["to"] += 1;

        file_put_contents("../datos/contadores.json", json_encode($contadores));
    }

    function update_contadores($from, $to) {
        update_uses($from, $to);
        update_total();
        update_percent();
    }
?>