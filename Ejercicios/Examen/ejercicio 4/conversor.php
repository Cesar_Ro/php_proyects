<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    $url = $_SERVER['PHP_SELF'];
    include "../datos/datos.php";
    include "./operaciones.php";

    $user = $_COOKIE['user'];
    $name = $_COOKIE["$user"][0];
    $date = $_COOKIE["$user"][1];

    echo "<h4>Usuario $user, último acceso $date</h4>";

    if ((isset($_GET['input']) && isset($_GET['origin']) && isset($_GET['change'])) || isset($_GET['refresh'])) {

        $importe = $_GET['input'];
        $origin = $_GET['origin'];
        $change = $_GET['change'];

        pintar($importe, $origin, $change);

        echo "<button><a href=\"./estadisticas.php\">Estadisticas</a></button>";
    }
    else {
        echo<<<EOT
<form action="$url" method="get">
    <label for="input">Importe</label>
    <input type="number" name="input">
    <label for="origin">De:</label>
    <select name="origin">
        <option value="eur">Euro</option>
        <option value="gbp">Libra</option>
        <option value="jpy">Yen</option>
        <option value="usd">Dolar Estadounidense</option>
        <option value="cad">Dolar Canadiense</option>
    </select>
    <label for="change">Para:</label>
    <select name="change">
        <option value="eur">Euro</option>
        <option value="gbp">Libra</option>
        <option value="jpy">Yen</option>
        <option value="usd">Dolar Estadounidense</option>
        <option value="cad">Dolar Canadiense</option>
    </select>
    <input type="submit" value="Convertir">
</form>
EOT;
    }
    ?>
</body>
</html>