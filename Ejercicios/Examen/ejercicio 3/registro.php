<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    include "../functions.php";

    if (isset($_POST['user']) && (isset($_POST['name']) && !empty($_POST['name'])) && (isset($_POST['pass']) && !empty($_POST['pass']))) {
        $name = $_POST['name'];
        $pass = $_POST['pass'];

        (empty($_POST['user'])) ? $result = (crea_usuario($name, $pass)) : $result = (crea_usuario($name, $pass, $_POST['user']));

        if ($result) {
            echo "<h3>$name registrado</h3>";
            header('Refresh:5; url=../index.php');
        }
        else {
            echo '<h3>Error en el registro</h3>';
            header('Refresh:5; url=../index.php');
        }
    }
    else {
        $url = $_SERVER['PHP_SELF'];
        echo<<<EOT
<form action="$url" method="post">
    <label for="user">Id usuario (opcional)</label>
    <input type="text" name="user"><br>
    <label for="name">Nombre</label>
    <input type="text" name="name"><br>
    <label for="pass">Contraseña</label>
    <input type="text" name="pass"><br><br>
    <input type="submit" value="Registrarse">
</form>
EOT;
    }
    ?>
</body>
</html>