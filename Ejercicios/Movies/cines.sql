use cines;

CREATE TABLE IF NOT EXISTS Cine (
    id int(10) PRIMARY KEY,
    name varchar(50),
    address varchar(255),
    postcode int(10),
    city varchar(50)
);

CREATE TABLE IF NOT EXISTS Pelicula (
    uid int(10) PRIMARY KEY,
    title varchar(255),
    sale varchar(255),
    image varchar(255),
    calification varchar(255),
    genre varchar(50),
    time int(10),
    summary varchar(1000),
    trailer varchar(255)
);

CREATE TABLE IF NOT EXISTS Sesion (
    cine_id int(10),
    sala_id int(10),
    pelicula_id int(10),
    fecha datetime,
    hora time,
    PRIMARY KEY (cine_id, sala_id, fecha, hora),
    CONSTRAINT fk_cine_id FOREIGN KEY (cine_id) REFERENCES Cine(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_pelicula_id FOREIGN KEY (pelicula_id) REFERENCES Pelicula(id) ON DELETE CASCADE ON UPDATE CASCADE
);