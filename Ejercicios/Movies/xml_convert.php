<?php
    require '../vendor/autoload.php';
    use Cines\CCine;
    use Cines\CSesion;
    use Cines\CPelicula;

    $xmlFile = @simplexml_load_file("Cartelera.xml");

    foreach ($xmlFile->recinto as $recinto) {
        $cine = new CCine([
            (int)$recinto['id'],
            (string)$recinto['value'],
            (string)$recinto->address,
            (int)$recinto->postcode,
            (string)$recinto->city
        ]);

        $result = $cine->insert();

        if (!$result[1]) {
            if ($result[0][0] == 23000) {
                $current = CCine::selectCine($cine->getId());
                if (!$current->compareCine($cine)) echo 'Id de cine duplicado';
            }
            else echo "Error al insertar cine id: {$cine->getId()} - {$result[0][0]} - {$result[0][2]} <br><br>";
        }

        foreach ($recinto->evento as $evento) {
            $pelicula = new CPelicula([
                (int)$evento->titulo['uid'],
                (string)$evento->titulo['value'],
                (string)$evento->compra,
                (string)$evento->caratula,
                (string)$evento->calificacion,
                (string)$evento->genero,
                (int)$evento->duracion,
                (string)$evento->sinopsis,
                (string)$evento->trailer
            ]);

            $result = $pelicula->insert();

            if (!$result[1]) {
                if ($result[0][0] == 23000) {
                    $current = CPelicula::selectPelicula($pelicula->getUid());
                    if (!$current->comparePelicula($pelicula)) echo "Error al insertar pelicula uid: {$pelicula->getUid()} - {$result[0][0]} - {$result[0][2]} <br><br>";
                }
                else echo "Error al insertar pelicula uid: {$pelicula->getUid()} - {$result[0][0]} - {$result[0][2]} <br><br>";
            }

            foreach ($evento->fechas->fecha as $day) {
                foreach ($day->sesiones->sala as $sesion) {

                }
            }
        }
    }
?>