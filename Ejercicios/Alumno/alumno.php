<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <?php
   $alumnos = array('1110'=>"Cesar Robres",'1111'=>"Javier Castel",'1112'=>"David Maldonado");

   if(!isset($_POST['x'])) {
    echo <<<EOT
<fieldset>
 <form action="./alumno.php" method="post">
  <label for="x">Número de expediente</label>
  <input type="number" name="x">
  <input type="submit" value="enviar información">
 </form>
</fieldset>
EOT;
   }

   else {
    $input = $_POST['x'];
    echo <<<EOT
<fieldset>
<p>$alumnos[$input]</p>
</fieldset>
EOT;
   }
  ?>  
 </body>
</html>