<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <fieldset>
    <?php
    include("../Functions.php");

    if(!isset($_POST['enviar']) || !control_entrada($_POST['name']) || !control_entrada($_POST['surname'])) {
     echo <<<EOT
<form action="./idiomas.php" method="post" enctype="multipart/form-data">
 <label for="name">Nombre</label>
 <input type="text" name="name">
 <br>

 <label for="surname">Apellidos</label>
 <input type="text" name="surname">
 <br>

 <label for="sex">Sexo</label>
 <input type="radio" name="sex" value="Varon"/>Varón
 <input type="radio" name="sex" value="Mujer"/>Mujer
 <br>

 <label for="age">Edad</label>
 <input type="number" name="age">
 <br><br>

 <label for="lang">Idiomas</label>
 <input type="checkbox" name="lang[]" value="español"/>Español
 <input type="checkbox" name="lang[]" value="ingles"/>Inglés
 <input type="checkbox" name="lang[]" value="frances"/>Francés
 <input type="checkbox" name="lang[]" value="italiano"/>Italiano
 <input type="checkbox" name="lang[]" value="portugues"/>Portugués
 <br><br>

 <label for="nation">Nacionalidad</label>
 <select name="nation">
  <option value="España">España</option>
  <option value="Reino Unido">Reino Unido</option>
  <option value="Estados Unidos">Estados Unidos</option>
  <option value="Francia">Francia</option>
  <option value="Italia">Italia</option>
  <option value="Portugal">Portugal</option>
 </select>
 <br>

 <label for="hobby">Aficciones</label>
 <select name="hobby">
  <option value="Pintura">Pintura</option>
  <option value="Deporte">Deportes</option>
  <option value="Videojuegos">Videojuegos</option>
  <option value="Lectura">Lectura</option>
 </select>
 <br><br>

 <input type="file" name="image">
 <br><br>

 <input type="submit" name="enviar" value="Enviar">
</form> 
EOT;
    }

    else {
     $name = $_POST['name'];
     $surname = $_POST['surname'];
     $sex = $_POST['sex'];
     $age = $_POST['age'];
     $lang = $_POST['lang'];
     $nation = $_POST['nation'];
     $hobby = $_POST['hobby'];

     echo<<<EOT
<table>
 <tr>
  <td>Nombre</td>
  <td>Apellido</td>
  <td>Sexo</td>
  <td>Edad</td>
  <td>Nacionalidad</td>
  <td>Hobby</td>
 </tr>
 <tr>
  <td>$name</td>
  <td>$surname</td>
  <td>$sex</td>
  <td>$age</td>
  <td>$nation</td>
  <td>$hobby</td>
 </tr>
 <tr>
  <td colspan="6">Idiomas</td>
 </tr>
 <tr>
EOT;
     foreach($lang as $valor) {
      echo "<td>$valor</td>";
     }
     echo '</tr></table>';
     
     $dir_name = "../Imagenes/";
     $file_name = time()."-".$name.$surname.".jpeg";
     $ruta_name = $dir_name.$file_name;
     move_uploaded_file($_FILES['image']['tmp_name'],$ruta_name);

     echo "<img src=\"$ruta_name\">";
    }
   ?>
  </fieldset>   
 </body>
</html>