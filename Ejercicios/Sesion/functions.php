<?php
$data_default = array(
    'name' => '',
    'surname' => '',
    'age' => '',
    'dni' => '',
    'city' => '',
    'post_code' => '',
    'country' => ''
);

function print_form($info) {
    $name = $info['name'];
    $surname = $info['surname'];
    $age = $info['age'];
    $dni = $info['dni'];
    $city = $info['city'];
    $post_code = $info['post_code'];
    $country = $info['country'];

    echo '<form action="." method="post" enctype="multipart/form-data">
    <label for="name"';
    if(!check_user($name) && $name != '') echo 'style="color:red"';
    echo<<<EOT
    >Nombre</label>
    <input type="text" name="name" value="$name">
    <br>

    <label for="surname">Apellidos</label>
    <input type="text" name="surname" value="$surname">
    <br>

    <label for="age"
    EOT;
    if(!check_age($age) && $age != '') echo 'style="color:red"';

    echo<<<EOT
    >Edad</label>
    <input type="number" name="age" value="$age">
    <br>

    <label for="dni"
    EOT;
    if(!check_dni($dni) && $dni != '') echo 'style="color:red"';

    echo<<<EOT
    >DNI</label>
    <input type="text" name="dni" value="$dni">
    <br>

    <label for="city">Ciudad</label>
    <input type="text" name="city" value="$city">
    <br>

    <label for="post_code">Codigo Postal</label>
    <input type="number" name="post_code" value="$post_code">
    <br>

    <label for="country">Pais</label>
    <input type="text" name="country" value="$country">
    <br><br>

    <label for="study">Estudios</label>
    <br>
    <input type="checkbox" name="study" value="eso"/>ESO
    <input type="checkbox" name="study" value="bachiller"/>Bachiller
    <input type="checkbox" name="study" value="fp"/>Formación profesional
    <input type="checkbox" name="study" value="grado"/>Grado
    <input type="checkbox" name="study" value="master"/>Master
    <br>

    <label for="lang">Idiomas</label>
    <br>
    <input type="checkbox" name="lang[]" value="español"/>Español
    <input type="checkbox" name="lang[]" value="ingles"/>Inglés
    <input type="checkbox" name="lang[]" value="frances"/>Francés
    <input type="checkbox" name="lang[]" value="italiano"/>Italiano
    <input type="checkbox" name="lang[]" value="portugues"/>Portugués
    <br><br>

    <input type="submit" name="send" value="Enviar">
   </form> 
   EOT;
}

function calc_letra($num) {
    $letras = array('T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E');
    return $letras[$num%23];
   }

function check_user($name) {
    if(preg_match('/[0-9]/', $name) && preg_match('/[A-Z]/', $name) && preg_match('/[a-z]/', $name)) return true;
    else return false;
}

function check_age($age) {
    return ($age > 17);
}

function check_dni($dni) {
    $letter = substr($dni, -1);
    $number = substr($dni, 0, -1);
    if((strlen($number) == 7 || strlen($number) == 8) && calc_letra($number) == $letter) return true;
    else return false;
}

function check_data($name, $age, $dni) {
    return (check_user($name) && check_age($age) && check_dni($dni));
}

function close_session() {
    $_SESSION=array();
    setcookie('PHPSESSID','',time()-3600);
    session_destroy();
    header('Location: index.php');
}

?>