<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
 </head>
 
 <body>
  <?php
  include "functions.php";
  session_start();

  if (!isset($_SESSION['name'])) { //¿No está ya acreditado?
    if(isset($_POST['name'])) { //¿Ha rellenado el formulario?
     if(check_data($_POST['name'], $_POST['age'], $_POST['dni'])) { //Son correctos los datos
      echo 'datos correctos';
      $_SESSION['name'] = $_POST['name'];
      $_SESSION['surname'] = $_POST['surname'];
      $_SESSION['age'] = $_POST['age'];
      $_SESSION['dni'] = $_POST['dni'];
      $_SESSION['city'] = $_POST['city'];
      $_SESSION['post_code'] = $_POST['post_code'];
      $_SESSION['country'] = $_POST['country'];
      $_SESSION['study'] = $_POST['study'];
      $_SESSION['lang'] = $_POST['lang'];
      header('Location: informacion.php');
     }
     
     else { //No son correctos los datos
      print_form($_POST);
     }
    }

    else { //No ha rellenado el formulario
     print_form($data_default);
    }
   }

   else { //Ya acreditado
    header('Location: informacion.php');
   }
  ?>
 </body>
</html>