<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <?php
   function print_calendar($mes) {
    $meses = [
      'enero' => 31,
      'febrero' => 28,
      'marzo' => 31,
      'abril' => 30,
      'mayo' => 31,
      'junio' => 30,
      'julio' => 31,
      'agosto' => 31,
      'septiembre' => 30,
      'octubre' => 31,
      'noviembre' => 30,
      'diciembre' => 31
    ];
    echo <<<EOT
  <table>
   <tr>
    <td class="name" colspan="7">$mes</td>
   </tr>
   <tr>
  EOT;
    for ($i = 1; $i <= $meses[strtolower($mes)]; $i++) {
     echo "<td>$i</td>";
     if($i%7==0) echo '</tr><tr>';
    }
    echo '</tr></table>';
   }

   if(!isset($_POST['x'])) {
    echo <<<EOT
<fieldset>
 <form action="./mes.php" method="post">
  <label for="x">Elige el mes</label>
  <select name="x">
   <option value="Enero">Enero</option>
   <option value="Febrero">Febrero</option>
   <option value="Marzo">Marzo</option>
   <option value="Abril">Abril</option>
   <option value="Mayo">Mayo</option>
   <option value="Junio">Junio</option>
   <option value="Julio">Julio</option>
   <option value="Agosto">Agosto</option>
   <option value="Septiembre">Septiembre</option>
   <option value="Octubre">Octubre</option>
   <option value="Noviembre">Noviembre</option>
   <option value="Diciembre">Diciembre</option>
  </select>
  <input type="submit" value="enviar información">
 </form>
</fieldset>
EOT;
   }
   else {
    $input = $_POST['x'];
    echo '<fieldset>';
    print_calendar($input);
    echo '<a class="return" href="./mes.php" class="boton">Volver</a>';
    echo '</fieldset>';
   }
  ?>  
 </body>
</html>