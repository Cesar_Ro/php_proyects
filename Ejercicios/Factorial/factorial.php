<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <?php
  function factorial($num) {
   $result = 1;
   for($num; $num > 1; $num--) {
    $result*=$num; 
    }
   return $result;
  }

   if(!isset($_POST['x'])) {
    echo <<<EOT
<fieldset>
 <form action="./factorial.php" method="post">
  <label for="x">Factorial de:</label>
  <input type="number" name="x" min="0" required>
  <input type="submit" value="enviar información">
 </form>
</fieldset>
EOT;
   }

   else {
    $input = $_POST['x'];
    $result = factorial($input);
    echo <<<EOT
<fieldset>
 <table>
  <tr>
   <td class="name">Factorial de $input</td>
  </tr>
  <tr>
   <td>$result</td>
  </tr>
 </table>
</fieldset>
EOT;
   }
  ?>  
 </body>
</html>