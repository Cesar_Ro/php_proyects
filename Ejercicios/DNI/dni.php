<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <?php
   function calc_letra($num) {
    $letras = array('T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E');
    return $letras[$num%23];
   }

   if(!isset($_REQUEST['x'])) {
    echo <<<EOT
<fieldset>
 <form action=$_SERVER['PHP_SELF'] method="post">
  <label for="x">Número del Dni</label>
  <input type="number" name="x" min="1000000" max="99999999" required>
  <input type="submit" value="enviar información">
 </form>
</fieldset>
EOT;
   }
   else {
    $numero = $_POST['x'];
    $result = (ctype_digit($numero)&&(strlen($numero)==7||strlen($numero)==8)) ? $numero.=calc_letra($numero) : "DATOS INCORRECTOS";
    echo <<<EOT
<p>El dni es</p><br>
<p class="respuesta">$result</p>
EOT;
   }
  ?>  
 </body>
</html>