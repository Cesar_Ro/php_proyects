<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="./style.css">
    <title>Document</title>
</head>
<body>
    <?php
    $usuarios = array(
    'cesar' => ['id123','123'],
    'david' => ['id124','321'],
    'juan' => ['id125','654']
    );

    if (isset($_POST['user']) && !empty($_POST['user'])) {
        $user = $_POST['user'];
        $pass = $_POST['password'];
        if (array_key_exists($user, $usuarios)) {
            if ($usuarios[$user][1] == $pass) echo "Bienvenid@ $user";
            else echo 'Acceso no autorizado';
        }
        else echo 'Usuario no existe';
    }
    ?>
    
</body>
</html>