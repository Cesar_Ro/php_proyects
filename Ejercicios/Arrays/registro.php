<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="./style.css">
  <title>Document</title>
 </head>
 <body>
  <?php
   if(isset($_POST['submit'])) {
    $user = $_POST['user'];
    $pass = $_POST['password'];
    $sex = $_POST['sex'];
    $lang = $_POST['lang'];
    $nation = $_POST['nation'];
    $hobby = $_POST['hobby'];

    echo<<<EOT
<table>
 <tr>
  <td>Nombre</td>
  <td>Apellido</td>
  <td>Sexo</td>
  <td>Nacionalidad</td>
  <td>Hobby</td>
 </tr>
 <tr>
  <td>$user</td>
  <td>$pass</td>
  <td>$sex</td>
  <td>$nation</td>
  <td>$hobby</td>
 </tr>
 <tr>
  <td colspan="6">Idiomas</td>
 </tr>
 <tr>
EOT;
     foreach($lang as $valor) {
      echo "<td>$valor</td>";
     }
     echo '</tr></table>';

   }
   else {
    echo<<<EOT
<form action="./registro.php" method="post">
 <label for="user">Usuario</label>
 <input type="text" name="user">
 <label for="password">Contraseña</label>
 <input type="text" name="password">

 <label for="sex">Sexo</label>
 <input type="radio" name="sex" value="Varon"/>Varón
 <input type="radio" name="sex" value="Mujer"/>Mujer
 <br>

 <label for="lang">Idiomas</label>
 <input type="checkbox" name="lang[]" value="español"/>Español
 <input type="checkbox" name="lang[]" value="ingles"/>Inglés
 <input type="checkbox" name="lang[]" value="frances"/>Francés
 <input type="checkbox" name="lang[]" value="italiano"/>Italiano
 <input type="checkbox" name="lang[]" value="portugues"/>Portugués
 <br><br>

 <label for="nation">Nacionalidad</label>
 <select name="nation">
  <option value="España">España</option>
  <option value="Reino Unido">Reino Unido</option>
  <option value="Estados Unidos">Estados Unidos</option>
  <option value="Francia">Francia</option>
  <option value="Italia">Italia</option>
  <option value="Portugal">Portugal</option>
 </select>
 <br>
   
 <label for="hobby">Aficciones</label>
 <select name="hobby">
  <option value="Pintura">Pintura</option>
  <option value="Deporte">Deportes</option>
  <option value="Videojuegos">Videojuegos</option>
  <option value="Lectura">Lectura</option>
 </select>
 <br><br>

 <input type="submit" name="submit" value="Registrar">
</form>
EOT;
   }
  ?>
  
 </body>
</html>