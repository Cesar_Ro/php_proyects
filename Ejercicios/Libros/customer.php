<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <title>Document</title>
</head>
<body>
<?php
    require '../vendor/autoload.php';
    use Clases\CHeader;
    use Libros\CCustomer;

    $config = json_decode(file_get_contents('config.json'), true);

    if(isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];
        $customer = CCustomer::selectCustomer($id);
        $header = new CHeader("Cliente {$customer->getId()} - {$customer->getFirstname()}", $config['header-primary'], $config['header-secondary']);
        echo $header;

        if(isset($_GET['edit']) && $_GET['edit'] == true) {
            echo<<<EOT
                <section class='center'>
                 <form action="controller.php" method="post">
                  <table>
                   <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Email</th>
                    <th>Tipo</th>
                   </tr>
                   <tr>
                    <td>{$customer->getId()}</td>
                    <input type='hidden' name='id' value='{$customer->getId()}'/>
                    <td><input type='text' name='firstname' value='{$customer->getFirstname()}'/></td>
                    <td><input type='text' name='surname' value='{$customer->getSurname()}'/></td>
                    <td><input type='text' name='email' value='{$customer->getEmail()}'/></td>
                    <td>
                     <select name="type">
                      <option value="basic">Basic</option>
                      <option value="premium">Premium</option>
                     </select>
                    </td>
                   </tr>
                  </table>
                  <input type="submit" name="customer-update" value="Confirmar">
                 </form>
                </section>
                <a href='list.php?list=customer'>Volver</a>
            EOT;
        }
        
        else {
            echo<<<EOT
                <section class='center'>
                 <table>
                  <tr>
                   <th>Id</th>
                   <th>Nombre</th>
                   <th>Apellido</th>
                   <th>Email</th>
                   <th>Tipo</th>
                  </tr>
                  <tr>
                   {$customer->htmlRow()}
                  </tr>
                 </table>
                 <a href='customer.php?id={$customer->getId()}&edit=true'>Editar</a>
                 <form action="controller.php" method="post">
                  <input type="hidden" name="id" value="{$customer->getId()}">
                  <input type="submit" name="customer-delete" value="Eliminar">
                 </form>
                 <a href='historic.php?type=customer&id={$customer->getId()}'>Historico</a>
                </section>
                <a href='list.php?list=customer'>Volver</a>
            EOT;
        }
    }
    else {
        header ('refresh: 2; url=./index.php');
        echo '<p>Acceso incorrecto</p>';
    }
    ?>
</body>
</html>