<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <title>Document</title>
</head>
<body>
<?php
    require '../vendor/autoload.php';
    use Clases\CHeader;
    use Clases\Conection;

    if (isset($_GET['list']) && !empty($_GET['list'])) {
        $conection = new Conection();
        $config = json_decode(file_get_contents('config.json'), true);

        switch ($_GET['list']) {
            case 'book':
                $header = new CHeader('Libros', $config['header-primary'], $config['header-secondary']);
                echo $header;

                echo '<section class="center">
                    <table>
                    <tr>
                    <th>Id</th>
                    <th>Isbn</th>
                    <th>Título</th>
                    <th>Autor</th>
                    <th>Stock</th>
                    <th>Precio</th>
                    <th></th>
                    </tr>';

                $stm = $conection->getConect()->query('SELECT * FROM book');
                while ($row = $stm->fetch()) {
                    echo "<tr>
                        <td>{$row['id']}</td>
                        <td>{$row['isbn']}</td>
                        <td>{$row['title']}</td>
                        <td>{$row['author']}</td>
                        <td>{$row['stock']}</td>
                        <td>{$row['price']}</td>
                        <td><a href='book.php?id={$row['id']}'>Detalles</a></td>
                        </tr>";
                }
                echo '</table>
                    </section>
                    <a href=\'input.php?new=book\'>Nuevo libro</a>
                    <a href=\'index.php\'>Volver</a>';
                break;

            case 'customer':
                $header = new CHeader('Clientes', $config['header-primary'], $config['header-secondary']);
                echo $header;

                echo '<section class="center">
                    <table>
                    <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Email</th>
                    <th>Tipo</th>
                    <th></th>
                    </tr>';

                $stm = $conection->getConect()->query('SELECT * FROM customer');
                while ($row = $stm->fetch()) {
                    echo "<tr>
                        <td>{$row['id']}</td>
                        <td>{$row['firstname']}</td>
                        <td>{$row['surname']}</td>
                        <td>{$row['email']}</td>
                        <td>{$row['type']}</td>
                        <td><a href='customer.php?id={$row['id']}'>Detalles</a></td>
                        </tr>";
                }
                echo '</table>
                    </section>
                    <a href=\'input.php?new=customer\'>Nuevo Cliente</a>
                    <a href=\'index.php\'>Volver</a>';
                break;

            case 'borrow':
                if (isset($_GET['type']) && !empty($_GET['type']) && isset($_GET['id']) && !empty($_GET['id'])) {
                    $id = $_GET['id'];
                    if ($_GET['type'] == 'customer') {
                        $nombre = 'Cliente';
                    }
                    $header = new CHeader("Historico de $nombre $id", $config['header-primary'], $config['header-secondary']);
                    echo $header;
                }
                else 
                break;

            default:
                header ('refresh: 2; url=./index.php');
                echo '<p>Acceso incorrecto</p>'; 
        }
    }
    else {
        header ('refresh: 2; url=./index.php');
        echo '<p>Acceso incorrecto</p>';  
    }
    ?>
</body>
</html>