<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <title>Document</title>
</head>
<body>
<?php
    require '../vendor/autoload.php';
    use Clases\CHeader;
    use Libros\CBook;
    use Clases\Conection;

    $conection = new Conection();
    $config = json_decode(file_get_contents('config.json'), true);

    if(isset($_GET['id']) && !empty($_GET['id'])) {
        
        $id = $_GET['id'];
        $book = CBook::selectBook($id);
        $header = new CHeader("Historico de Libro {$book->getId()}", $config['header-primary'], $config['header-secondary']);
        echo $header;

        echo<<<EOT
<table class='borrow'>
 <tr>
  <th>Cliente</th>
  <th>Prestamo</th>
  <th>Límite devolución</th>
 </tr>
EOT;
        $stm = $conection->getConect()->query("SELECT customer_id, start, end FROM borrowed_books WHERE book_id={$book->getId()}");

        while ($row = $stm->fetch()) {
            $stmCustomer = $conection->getConect()->query("SELECT firstname, surname FROM customer WHERE id = {$row['customer_id']}");
            $customer = $stmCustomer->fetch();
            $name = $row['customer_id'].' - '.$customer['firstname'].' '.$customer['surname'];
            $start = substr($row['start'], 0, 10);
            $end = substr($row['end'], 0, 10);
            echo<<<EOT
<tr>
 <td>$name</td>
 <td>$start</td>
 <td>$end</td>
</tr>
EOT;
        }

        echo<<<EOT
</table>
<table class='sale'>
 <tr>
  <th>Cliente</th>
  <th>Fecha</th>
  <th>Cantidad</th>
 </tr>
EOT;

        $stm = $conection->getConect()->query("SELECT sale_id, amount FROM sale_book WHERE book_id={$book->getId()}");

        while ($row = $stm->fetch()) {
            $stmSale = $conection->getConect()->query("SELECT customer_id, date FROM sale WHERE id={$row['sale_id']}");
            $sale = $stmSale->fetch();
            $date = substr($sale['date'], 0, 10);
            $stmCustomer = $conection->getConect()->query("SELECT firstname, surname FROM customer WHERE id = {$sale['customer_id']}");
            $customer = $stmCustomer->fetch();
            $name = $sale['customer_id'].' - '.$customer['firstname'].' '.$customer['surname'];
            echo<<<EOT
<tr>
 <td>$name</td>
 <td>$date</td>
 <td>{$row['amount']}</td>
</tr>
EOT;
        }
        echo '</table>';
    }
    $conection = null;
    ?>
</body>
</html>