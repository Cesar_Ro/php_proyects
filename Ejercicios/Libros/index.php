<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <title>Document</title>
</head>
<body>
    <?php
    require '../vendor/autoload.php';
    use Clases\CHeader;

    $config = json_decode(file_get_contents('config.json'), true);

    $header = new CHeader('Librería', $config['header-primary'], $config['header-secondary']);
    echo $header;

    echo '<a href=\'list.php?list=customer\'>Ver Clientes</a>';
    echo '<a href=\'list.php?list=book\'>Ver Libros</a>';
    ?>    
</body>
</html>