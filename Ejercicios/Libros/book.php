<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <title>Document</title>
</head>
<body>
<?php
    require '../vendor/autoload.php';
    use Clases\CHeader;
    use Libros\CBook;

    $config = json_decode(file_get_contents('config.json'), true);

    if(isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];
        $book = CBook::selectBook($id);
        $header = new CHeader("Libro {$book->getId()} - {$book->getTitle()}", $config['header-primary'], $config['header-secondary']);
        echo $header;

        if(isset($_GET['edit']) && $_GET['edit'] == true) {
            echo<<<EOT
                <section class='center'>
                 <form action="controller.php" method="post">
                  <table>
                   <tr>
                    <th>Id</th>
                    <th>Isbn</th>
                    <th>Título</th>
                    <th>Autor</th>
                    <th>Stock</th>
                    <th>Precio</th>
                   </tr>
                   <tr>
                    <td>{$book->getId()}</td>
                    <input type='hidden' name='id' value='{$book->getId()}'/>
                    <td><input type='text' name='isbn' value='{$book->getIsbn()}'/></td>
                    <td><input type='text' name='title' value='{$book->getTitle()}'/></td>
                    <td><input type='text' name='author' value='{$book->getAuthor()}'/></td>
                    <td><input type='text' name='stock' value='{$book->getStock()}'/></td>
                    <td><input type='text' name='price' value='{$book->getPrice()}'/></td>
                   </tr>
                  </table>
                  <input type="submit" name="book-update" value="Confirmar">
                 </form>
                </section>
                <a href='list.php?list=book'>Volver</a>
            EOT;
        }
        
        else {
            echo<<<EOT
                <section class='center'>
                 <table>
                  <tr>
                   <th>Id</th>
                   <th>Isbn</th>
                   <th>Título</th>
                   <th>Autor</th>
                   <th>Stock</th>
                   <th>Precio</th>
                  </tr>
                  <tr>
                   {$book->htmlRow()}
                  </tr>
                 </table>
                 <a href='book.php?id={$book->getId()}&edit=true'>Editar</a>
                 <form action="controller.php" method="post">
                  <input type="hidden" name="id" value="{$book->getId()}">
                  <input type="submit" name="book-delete" value="Eliminar">
                 </form>
                 <a href='historic.php?type=book&id={$book->getId()}'>Historico</a>
                 <a href=\'list.php?list=borrow&type='customer'&id={$book->getId()}\'>Historico</a>
                </section>
                <a href='list.php?list=book'>Volver</a>
            EOT;
        }
    }
    else {
        header ('refresh: 2; url=./index.php');
        echo '<p>Acceso incorrecto</p>';
    }
    ?>
</body>
</html>