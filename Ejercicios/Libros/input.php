<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <title>Document</title>
</head>
<body>
    <?php
    require '../vendor/autoload.php';

    use Libros\CBook;
    use Libros\CCustomer;
    use Clases\CHeader;

    $config = json_decode(file_get_contents('config.json'), true);

    if (isset($_GET['new']) && !empty($_GET['new'])) {
        switch ($_GET['new']) {
            case 'book':
                $header = new CHeader('Nuevo libro', $config['header-primary'], $config['header-secondary']);
                echo $header;
                echo CBook::inputForm();
                break;
            case 'customer':
                $header = new CHeader('Nuevo cliente', $config['header-primary'], $config['header-secondary']);
                echo $header;
                echo CCustomer::inputForm();
                break;
            case 'borrow':
                $header = new CHeader('Nuevo prestamo', $config['header-primary'], $config['header-secondary']);
                echo $header;
                echo "i es igual a 2";
                break;
            case 'sale':
                $header = new CHeader('Nueva venta', $config['header-primary'], $config['header-secondary']);
                echo $header;
                echo "i es igual a 3";
                break;
            default:
                header ('refresh: 2; url=./index.php');
                echo '<p>Acceso incorrecto</p>'; 
        }
    }
    else {
        header ('refresh: 2; url=./index.php');
        echo '<p>Acceso incorrecto</p>';
    }
    ?>
</body>
</html>