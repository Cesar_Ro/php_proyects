use libros;

CREATE TABLE IF NOT EXISTS Book (
    id int(10) PRIMARY KEY,
    isbn varchar(13),
    title varchar(255),
    author varchar(255),
    stock smallint(5),
    price float
);
CREATE TABLE IF NOT EXISTS Customer (
    id int(10) PRIMARY KEY,
    firstname varchar(255),
    surname varchar(255),
    email varchar(255),
    type enum('basic', 'premium')
);
CREATE TABLE IF NOT EXISTS Sale (
    id int(10) PRIMARY KEY,
    customer_id int(10),
    date datetime,
    CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES Customer(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS Borrowed_books (
    customer_id int(10),
    book_id int(10),
    start datetime,
    end datetime,
    PRIMARY KEY (customer_id, book_id),
    CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES Customer(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_book_id FOREIGN KEY (book_id) REFERENCES Book(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS Sale_book (
    book_id int(10),
    sale_id int(10),
    amount smallint(5),
    PRIMARY KEY (book_id, sale_id),
    CONSTRAINT fk_book_id FOREIGN KEY (book_id) REFERENCES Book(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_sale_id FOREIGN KEY (sale_id) REFERENCES Sale(id) ON DELETE CASCADE ON UPDATE CASCADE
);