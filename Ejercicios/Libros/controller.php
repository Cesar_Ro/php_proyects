<?php
require '../vendor/autoload.php';

use Libros\CBook;
use Libros\CCustomer;
use Clases\CHeader;

$mensaje = 'Acceso incorrecto';

// Insertar, actualizar y borrar libros

if (isset($_POST['book-insert']) || isset($_POST['book-update'])) {
    $value = [
        $_POST['isbn'],
        $_POST['title'],
        $_POST['author'],
        $_POST['stock'],
        $_POST['price']
    ];
    $book = new CBook($value);

    if (isset($_POST['book-insert'])) {
        $return = $book->insert();
        if ($return) $mensaje = 'Libro creado';
        else $mensaje = 'Error creando libro';
    }
    else if (isset($_POST['book-update'])) {
        $book->setId($_POST['id']);
        $return = $book->update();
        if ($return) $mensaje = 'Libro actualizado';
        else $mensaje = 'Error actualizando libro';
    }
}

else if (isset($_POST['book-delete'])) {
    $book = CBook::selectBook($_POST['id']);
    $return = $book->delete();
    if ($return) $mensaje = 'Cliente eliminado';
    else $mensaje = 'Error eliminando cliente';
}

// Insertar, actualizar y borrar clientes

else if (isset($_POST['customer-insert']) || isset($_POST['customer-update'])) {
    $value = [
        $_POST['firstname'],
        $_POST['surname'],
        $_POST['email'],
        $_POST['type'],
    ];
    $customer = new CCustomer($value);

    if (isset($_POST['customer-insert'])) {
        $return = $customer->insert();
        if ($return) $mensaje = 'Cliente creado';
        else $mensaje = 'Error creando cliente';
    }
    else if (isset($_POST['customer-update'])) {
        $customer->setId($_POST['id']);
        $return = $customer->update();
        if ($return) $mensaje = 'Cliente actualizado';
        else $mensaje = 'Error actualizando cliente';
    }
}

else if (isset($_POST['customer-delete'])) {
    $customer = CCustomer::selectCustomer($_POST['id']);
    $return = $customer->delete();
    if ($return) $mensaje = 'Cliente eliminado';
    else $mensaje = 'Error eliminando cliente';
}

// Insertar, actualizar y borrar prestamos

else if (isset($_POST['borrow-insert']) || isset($_POST['borrow-update'])) {
    echo 'nuevo prestamo';
}

// Insertar, actualizar y borrar ventas

else if (isset($_POST['sale-insert']) || isset($_POST['sale-update'])) {
    echo 'nueva venta';
}

header ('refresh: 2; url=./index.php');
echo "<p>$mensaje</p>";
?>