<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Tablas</title>
 </head>
 <body>
  <table>
   <?php
   $temperaturas = array();
   $temperaturas['Caja_1'] = array(1,1,2,3,2,1,2,3,3,3,2,1,3,4);
   $temperaturas['Caja_2'] = array(0,0,3,2,4,3,2,0,1,2,3,4,2,1);
   $temperaturas['Caja_3'] = array(3,1,2,3,5,2,2,0,1,2,3,4,2,1);
   $temperaturas['Caja_4'] = array(2,2,2,3,5,2,3,2,0,1,2,3,0,1);
   $temperaturas['Caja_5'] = array(0,3,2,3,5,2,3,2,0,1,2,3,0,1);

   function over($data,$limit) {
    return ($data>$limit);
   }
   Define('TEMPALERT',4);

   foreach ($temperaturas as $indice => $valor) {
    echo <<<EOT
<tr>
<td class="name" colspan="14">$indice</td>
</tr>
<tr>
EOT;
    foreach ($valor as $indice2 => $valor2) {
     echo '<td';
     $alert = (over($valor2,TEMPALERT)) ? 'class="alert"' : '';
     echo " $alert>$valor2</td>\n";
    }
    echo "</tr>\n";
   }
   ?>
  </table>
 </body>
</html>