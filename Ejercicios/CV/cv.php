<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="./cv_style.css">
  <title>Document</title>
 </head>
 <body>
  <?php
   if(!isset($_POST['end'])) {
   echo<<<'EOT'
<form action="./cv.php" method="post" enctype="multipart/form-data">
 <fieldset>
  <legend>Datos Personales</legend>
  <label for="name">Nombre</label>
  <input type="text" name="name">
  <label for="surname">Apellidos</label>
  <input type="text" name="surname">
  <label for="image">Foto</label>
  <input type="file" name="photo">
 </fieldset>
 <fieldset>
  <legend>Datos Contacto</legend>
  <label for="email">Email</label>
  <input type="email" name="email">
  <label for="phone">Teléfono</label>
  <input type="tel" name="phone" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" placeholder="123-123-123">
 </fieldset>
 <fieldset>
  <legend>Aptitudes</legend>
  <ol>
   <li><input type="text" name="apt[]"></li>
   <li><input type="text" name="apt[]"></li>
   <li><input type="text" name="apt[]"></li>
   <li><input type="text" name="apt[]"></li>
   <li><input type="text" name="apt[]"></li>
   <li><input type="text" name="apt[]"></li>
  </ol>
 </fieldset>
 <fieldset>
  <legend>Formación</legend>
  <fieldset>
   <legend>Formación 1</legend>
   <label for="tittle_1">Título</label>
   <input type="text" name="tittle_1">
   <label for="center_1">Centro de estudios</label>
   <input type="text" name="center_1">
   <label for="start_time_1">Inicio</label>
   <input type="text" name="start_time_1" size="4" pattern="[0-9]{4}">
   <label for="end_time_1">Fín</label>
   <input type="text" name="end_time_1" size="4" pattern="[0-9]{4}">
  </fieldset>
  <fieldset>
   <legend>Formación 2</legend>
   <label for="tittle_2">Título</label>
   <input type="text" name="tittle_1">
   <label for="center_2">Centro de estudios</label>
   <input type="text" name="center_2">
   <label for="start_time_2">Inicio</label>
   <input type="text" name="start_time_2" size="4" pattern="[0-9]{4}">
   <label for="end_time_2">Fín</label>
   <input type="text" name="end_time_2" size="4" pattern="[0-9]{4}">
  </fieldset>
  <fieldset>
   <legend>Formación 3</legend>
   <label for="tittle_3">Título</label>
   <input type="text" name="tittle_1">
   <label for="center_3">Centro de estudios</label>
   <input type="text" name="center_3">
   <label for="start_time_3">Inicio</label>
   <input type="text" name="start_time_3" size="4" pattern="[0-9]{4}">
   <label for="end_time_3">Fín</label>
   <input type="text" name="end_time_3" size="4" pattern="[0-9]{4}">
  </fieldset>
 </fieldset>
 <fieldset>
  <legend>Experiencia</legend>
  <fieldset>
   <legend>Experiencia 1</legend>
   <label for="company_1">Empresa</label>
   <input type="text" name="company_1">
   <label for="post_1">Puesto</label>
   <input type="text" name="post_1">
   <label for="comp_start_1">Inicio</label>
   <input type="month" name="comp_start_1">
   <label for="comp_end_1">Fín</label>
   <input type="month" name="comp_end_1">
  </fieldset>
  <fieldset>
   <legend>Experiencia 2</legend>
   <label for="company_2">Empresa</label>
   <input type="text" name="company_2">
   <label for="post_2">Puesto</label>
   <input type="text" name="post_2">
   <label for="comp_start_2">Inicio</label>
   <input type="month" name="comp_start_2">
   <label for="comp_end_2">Fín</label>
   <input type="month" name="comp_end_2">
  </fieldset>
  <fieldset>
   <legend>Experiencia 3</legend>
   <label for="company_3">Empresa</label>
   <input type="text" name="company_3">
   <label for="post_3">Puesto</label>
   <input type="text" name="post_3">
   <label for="comp_start_3">Inicio</label>
   <input type="month" name="comp_start_3">
   <label for="comp_end_3">Fín</label>
   <input type="month" name="comp_end_3">
  </fieldset>
 </fieldset>
 <fieldset>
  <legend>Resumen Profesional</legend>
  <textarea name="resume" rows="5" cols="100"></textarea>
 </fieldset>
 <input type="submit" name="end">
</form>
EOT;
   }
   else {
    $name = $_POST['name']." ".$_POST['surname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $apt = $_POST['apt'];
    $resume = $_POST['resume'];

    echo<<<EOT
<div class="container">
 <div class="data_left">
  <div class="container_image">
   <img class="image" src="./foto_perfil.jpeg">
  </div>
  <div class="container_resume">
   <div class="resume_tittle">Resumen Profesional</div>
   <div class="resume_text">$resume</div>
  </div>
  <div class="container_apt">
   <div class="apt_tittle">Aptitudes</div>
   <div class="apt_group">
    <ol>
EOT;

    foreach ($apt as $valor) {
     echo "<li class=\"apt\">$valor</li>";
    }

    echo<<<EOT
    </ol>
   </div>
  </div>
 </div>
 <div class="data_right">
  <div class="container_contact">
   <div class="contact_name">$name</div>
   <div class="contact_email">Email: $email</div>
   <div class="contact_phone">Tlf: $phone</div>
  </div>
  <div class="container_formacion">
  </div>
 </div>
</div>
EOT;
   }
  ?>
 </body>
</html>