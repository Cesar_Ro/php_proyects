<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="./baraja.css">
  <title>Document</title>
 </head>
 <body>
  <fieldset>
   <table>
    <?php
     include "./functions.php";

     if(isset($_POST['baraja'])) {
      if ($POST['baraja'] == 0) $baraja = $barajaEspañola;
      else if ($_POST['baraja'] == 1) $baraja = $barajaFrancesa;
      foreach($baraja as $palo => $valor) {
       echo '<tr>';
       foreach($valor as $carta) {
        $img = "./img/$carta$palo.jpg";
        echo "<td><img src=\"$img\"></td>";
       }
       echo '</tr>';
      }
     }
    ?>
   </table>
  </fieldset>
 </body>
</html>