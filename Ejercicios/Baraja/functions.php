<?php
$barajaEspañola = array(
    'oros' => $oros = range(1,12),
    'bastos' => $bastos = range(1,12),
    'copas' => $copas = range(1,12),
    'espadas' => $espadas = range(1,12)
    );

$barajaFrancesa = array(
    'corazones' => $corazones = range(1,12),
    'diamantes' => $diamantes = range(1,12),
    'treboles' => $treboles = range(1,12),
    'picas' => $picas = range(1,12)
    );

function seleccionaCartas($numCartas, $baraja) {
 $result = array();
 for($i = 0; $i < $numCartas; $i++) {
  $carta = randomCarta($baraja);
  array_push($result, $carta);
 }
 return $result;
}

function randomCarta($baraja) {
 $palo = array_rand($baraja);
 $carta = $baraja[$palo][array_rand($baraja[$palo])];
 $result = "$carta$palo";
 return $result;
}
?>