<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    require '../vendor/autoload.php';

    $header = new Clases\CHeader('Zayas', 'red', 'green');
    echo $header;

    $person_1 = new Clases\CCustomer('Juan','Pedro', 'j_pedro@email', 'Calle ficticia 2');
    echo $person_1;

    ?>
</body>
</html>