<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <?php
   include "../Functions.php";

   if(isset($_POST['year']) && !empty($_POST['year'])) {
    if(isset($_POST['month']) && !empty($_POST['month'])) {
     print_month($_POST['year'], $_POST['month']);
    }
    else print_year($_POST['year']); 
   }

   else {
    echo<<<EOT
<fieldset>
 <form action="./calendario.php" method="post">
  <input type="number" name="year" value="Año">
  <select name="month">
   <option></option>
   <option value="1">Enero</option>
   <option value="2">Febrero</option>
   <option value="3">Marzo</option>
   <option value="4">Abril</option>
   <option value="5">Mayo</option>
   <option value="6">Junio</option>
   <option value="7">Julio</option>
   <option value="8">Agosto</option>
   <option value="9">Septiembre</option>
   <option value="10">Octubre</option>
   <option value="11">Noviembre</option>
   <option value="12">Diciembre</option>
  </select>
  <input type="submit">
 </form>
</fieldset>
EOT;
echo date("");
   }
  ?>   
 </body>
</html>