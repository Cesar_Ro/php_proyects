<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <fieldset>
   <?php
    $usuario = [
     'admin' => "1234",
     'user_1' => "number1",
     'user_2' => "number2"
     ];

    if(!isset($_GET['x']) && !isset($_GET['y'])) {
     echo <<<EOT
 <form action="./control_de_acceso.php" method="get">
  <label for="x">Nombre de usuario</label>
  <input type="text" name="x">
  <br>
  <label for="x">Contraseña</label>
  <input type="password" name="y">
  <br><br>
  <input type="submit" value="enviar información">
 </form>
EOT;
    }

    else {
     $user = $_GET['x'];
     $pass = $_GET['y'];
     if($usuario[$user] == $pass) echo "<p class=\"correct\">¡Bienvenido $user!</p";
     else echo '<p class="alert">Usuario no autorizado</p>';
    }
   ?>  
  </fieldset>
 </body>
</html>