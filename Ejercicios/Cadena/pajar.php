<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <?php   


   function buscar($cadena,$word) {
    if (!existe($cadena, $word)) return false;
    else {
     $long = strlen($word);
     $pos = 0;
     $result = array();
     while (($posicion = stripos($cadena, $word)) !== false) {
      $result[] = $pos + $posicion;
      $pos += ($posicion + $long);
      $cadena = substr($cadena, ($posicion + $long));
     }
     return $result;
    }
   }

   function existe($cadena,$word) {
    if (stripos($cadena, $word) === false) return false;
    else return true;
   }



   if(isset($_POST['text']) && !empty($_POST['text'])) {
    $text = $_POST['text'];
    $text = str_replace('<span class="correct">','',$text);
    $text = str_replace('</span>','',$text);
   }
   else $text = 'Irene Ana Palma Ana Sergio Pedro Ana';


   if(isset($_POST['boton'])) {
    if(isset($_POST['origin']) && !empty($_POST['origin'])) {
     $origin = $_POST['origin'];
     
     if (existe($text, $origin)) {
      if($_POST['boton'] == 'Buscar') {
       $word = '<span class="correct">'.$origin.'</span>';  
      }
      else if($_POST['boton'] == 'Reemplazar' && (isset($_POST['change']) && !empty($_POST['change']))) {
       $word = $_POST['change'];  
      }
      else if($_POST['boton'] == 'Eliminar') {
       $word = "";
      }

      if(isset($word)) {
        $text = str_ireplace($origin, $word, $text);
        $mensaje = "Operación realizada";
       }
       else $mensaje = 'Sin palabra para reemplazo';
     }
     else $mensaje = 'Palabra no encontrada';
    }
    else $mensaje = 'Sin parametro de busqueda';


    echo<<<EOT
<fieldset>
 <form action="./pajar.php" method="post">
  <p>$text</p>
  <input type="hidden" name="text" value='$text'>
  <br>
  <input type="text" name="origin">
  <input type="text" name="change">
  <br>
  <input type="submit" name="boton" value="Buscar">
  <input type="submit" name="boton" value="Reemplazar">
  <input type="submit" name="boton" value="Eliminar">
  <input type="submit" name="nuevo" value="Nuevo Texto">
  <p>$mensaje</p>
 </form>
</fieldset>
EOT;
   }

   else {
    echo<<<EOT
<fieldset>
 <form action="./pajar.php" method="post">
  <textarea name="text" placeholder='Irene Ana Palma Ana Sergio Pedro Ana'></textarea>
  <br>
  <input type="text" name="origin">
  <input type="text" name="change">
  <br>
  <input type="submit" name="boton" value="Buscar">
  <input type="submit" name="boton" value="Reemplazar">
  <input type="submit" name="boton" value="Eliminar">
 </form>
</fieldset>
EOT;
   }
  ?>
 </body>
</html>