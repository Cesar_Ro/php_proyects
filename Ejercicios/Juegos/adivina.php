<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
   <?php
    $correcto = rand(0, 10);
    $numero = (isset($_GET["x"])) ? $_GET["x"] : "";
    $result = (is_numeric($numero) && $numero == $correcto) ? '<fieldset class="correct">CORECTO' : '<fieldset class="alert">EQUIVOCADO';
    echo <<<EOT
$result<br><p class="respuesta">$correcto</p>
EOT;
   ?>
  </fieldset>
 </body>
</html>