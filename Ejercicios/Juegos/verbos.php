<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
  <title>Document</title>
 </head>
 <body>
  <fieldset>
   <?php
    include "../Datos/array_verbos_asociativo.php";

    if(isset($_POST['fin']) || isset($_POST['siguiente'])) {
     $repeticiones = $_POST['repeticiones'] + 1;
     $aciertos = $_POST['aciertos'];
     $fallos = $_POST['fallos'];
     $verbo = $_POST['verbo'];
     $string_verbos = $_POST['concat'];

     ($_POST['presente']==$array_verbos[$verbo][0]) ? $aciertos++ : $fallos++;
     ($_POST['pasado']==$array_verbos[$verbo][1]) ? $aciertos++ : $fallos++;
     ($_POST['participio']==$array_verbos[$verbo][2]) ? $aciertos++ : $fallos++;
     $string_verbos.=$verbo;
     $array_verb_asked = explode(",", $string_verbos);
    }

    else {
     $repeticiones = 0;
     $aciertos = 0;
     $fallos = 0;
     $string_verbos = "";   
    }

    if(!isset($_POST['fin']) && $repeticiones < count($array_verbos)) {
     if(isset($_POST['siguiente'])) {
      for($i = 0; $i < count($array_verb_asked); $i++) {
       unset($array_verbos[$array_verb_asked[$i]]);
      }
      $string_verbos.=",";
     }

     $verbo = array_rand($array_verbos);

     echo <<<EOT
<p>$verbo</p>
<form action="./verbos.php" method="post">
 <label for="presente">Presente</label>
 <input type="text" name="presente">
 <label for="pasado">Pasado</label>
 <input type="text" name="pasado">
 <label for="participio">Participio</label>
 <input type="text" name="participio">
 <input type="hidden" name="repeticiones" value="$repeticiones">
 <input type="hidden" name="aciertos" value="$aciertos">
 <input type="hidden" name="fallos" value="$fallos">
 <input type="hidden" name="verbo" value="$verbo">
 <input type="hidden" name="concat" value="$string_verbos">
 <br><br>
 <input type="submit" name="siguiente" value="Siguiente">
 <input type="submit" name="fin" value="Finalizar">
</form>
EOT;
    }
    
    else {
        $string_verbos = str_replace(",",", ",$string_verbos);
     echo <<<EOT
<p>Preguntas: $repeticiones <br>
Aciertos: $aciertos <br>
Fallos: $fallos <br><br>
Verbos: $string_verbos</p>
EOT;
    }
   ?>
  </fieldset>
 </body>
</html>