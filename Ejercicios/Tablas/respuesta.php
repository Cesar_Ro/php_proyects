<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="x-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" media="screen" href="css\style.css">
  <title>Document</title>
 </head>
 <body>
  <fieldset>
   <?php

    function print_mult($num) {
     echo <<<EOT
<table>
<tr>
<td colspan="2" class="name">Tabla del $num</td>
</tr>
EOT;
     for ($i = 0; $i <= 10; $i++) {
      $result = $num * $i;
      echo <<<EOT
<tr>
<td>$num * $i</td>
<td>$result</td>
</tr>
</table>
EOT;
     }
    }

    $numero = (isset($_GET["x"])) ? $_GET["x"] : "";
    if (is_numeric($numero)) tab_mult($numero);
    else if ($numero=="") echo '<tr><td class="alert">VARIABLE NO RECIBIDA</td></tr>';
    else echo '<tr><td class="alert">VARIABLE NO VÁLIDA</td></tr>';
   ?>
  </fieldset>
 </body>
</html>
