<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" type="text/css" media="screen" href="style.css">
  <title>Tablas</title>
 </head>
 <body>
  <?php 
  define('MAXTAB', 10);
  define('MAXNUM', 10);
  for ($i = 1; $i <= MAXTAB; $i++) {
    echo "<div>\n
      <table>\n
      <thead>\n
      <tr>\n
      <td colspan=\"2\">TABLA DEL $i</td>\n
      </tr>\n
      </thead>\n";
    for ($j = 1; $j <= MAXNUM; $j++) {
        $result = $i * $j;
        echo "<tr>\n
          <td>$i * $j</td>\n
          <td>$result</td>\n
          </tr>\n";
    }
    echo "</table>\n
      <br>\n
      </div>\n";
  } 
  ?>
 </body>
</html>