<?php

//Imprimir tabla de multiplicar

 function print_mult($num) {
  echo <<<EOT
<table>
<tr>
<td colspan="2" class="caja_name">Tabla del $num</td>
</tr>
EOT;
  for ($i = 0; $i <= 10; $i++) {
   $result = $num * $i;
   echo <<<EOT
<tr>
<td>$num * $i</td>
<td>$result</td>
</tr>
</table>
EOT;
  }
 }

//Imprimir calendario mensual

 function print_month($year, $month) {
  $day = 1;
  $date = mktime(0, 0, 0, $month, $day, $year);
  $weekday = date("N", $date);
  $month_name = date("F", $date);

  echo<<<EOT
<table>
 <tr>
  <td class="name" colspan="7">$month_name de $year</td>
 </tr>
 <tr>
  <td class="subname">L</td>
  <td class="subname">M</td>
  <td class="subname">X</td>
  <td class="subname">J</td>
  <td class="subname">V</td>
  <td class="subname">S</td>
  <td class="subname">D</td>
 </tr>
 <tr>
EOT;

  for ($i = 1; $i < $weekday; $i++) {
   echo "<td> </td>";
  }

  while (checkdate($month, $day, $year)) {
   echo "<td>$day</td>";
   $day++;
   $weekday++;
   if($weekday > 7) {
    echo '</tr><tr>';
    $weekday = 1;
   }
  }

  for ($weekday; $weekday <= 7; $weekday++) {
   echo "<td> </td>";
  }

  echo '</tr></table>';
 }

//Imprimir calendario anual

 function print_year($year) {
  echo<<<EOT
<table>
 <tr>
  <td class="name" colspan="4">$year</td>
 </tr>
 <tr>
EOT;

  for ($month = 1; $month <= 12; $month++) {
   echo '<td>';
   print_month($year, $month);
   if ($month%4 == 0) echo "</tr><tr>";
   echo '</td>';
  }
  echo "</tr></table>";
 }

//Obtener letra del DNI

 function calc_letra($num) {
  $letras = array('T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E');
  return $letras[$num%23];
 }

//Control de entrada input type text

 function control_entrada ($valor) {
  return (isset($valor) && !empty($valor));
 }
?>