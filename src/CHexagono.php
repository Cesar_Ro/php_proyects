<?php

namespace Figuras;

class CHexagono extends CFigura {
    private $side;

    public function __construct($side) {
        $this->side = $side;
        parent::__construct(CHexagono::calculateArea(), CHexagono::calculatePerimeter());
        $this->image = CHexagono::createImage();
    }

    private function calculateArea() {
        $this->perimeter = CHexagono::calculatePerimeter();
        $apotema = sqrt(pow($this->side, 2) - pow($this->side / 2, 2));
        return ($this->perimeter * $apotema) / 2;
    }

    private function calculatePerimeter() {
        return $this->side * 6;
    }

    private function createImage() {
        $image = imagecreatetruecolor(500, 500);

        $grey_dark = imagecolorallocate($image, 150, 150, 150);
        $red = imagecolorallocate($image, 255, 0, 0);
        $black = imagecolorallocate($image, 0, 0, 0);

        imagefill($image, 0, 0, $grey_dark);

        $center = 250;
        $side = $this->side * 15;
        
        imagefilledarc($image, $center, $center, $side, $side, 0, 60, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 60, 120, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 120, 180, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 180, 240, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 240, 300, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 300, 0, $red, IMG_ARC_CHORD);

        imagefilledarc($image, $center, $center, $side, $side, 0, 60, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 60, 120, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 120, 180, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 180, 240, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 240, 300, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 300, 0, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);

        return $image;
    }

    public static function inputForm() {
        return '<section>
        <form action="showFigure.php" method="post">
         <div class="select">
          <input type="number" name="side"/>Lado
          <input type="hidden" name="fig" value="Hexagono">
         </div>
         <div class="enviar">
          <input type="submit" name="btn-data" value="Seleccionar">
         </div>
        </form>
       </section>';
    }

    public function __toString() {
        return "<p>Lado: $this->side</p>".parent::__toString();
    }
}

?>