<?php

namespace Libros;
require '../vendor/autoload.php';
use Clases\Conection;

class CBook extends Conection {
    private $id;
    private $isbn;
    private $title;
    private $author;
    private $stock;
    private $price;

    public function __construct($array) {
        $this->isbn = $array[0];
        $this->title = $array[1];
        $this->author = $array[2];
        $this->stock = $array[3];
        $this->price = $array[4];
        parent::__construct();
        $this->id = CBook::newId();
    }

// Métodos Get y Set

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getIsbn() {
        return $this->isbn;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getAuthor() {
        return $this->author;
    }
    public function getStock() {
        return $this->stock;
    }
    public function getPrice() {
        return $this->price;
    }

// Formulario input

    public static function inputForm() {
        return '<section class="center">
        <form action="controller.php" method="post">
         <div class="select">
          <input type="text" name="isbn"/>Isbn
          <input type="text" name="title"/>Título
          <input type="text" name="author"/>Autor
          <input type="text" name="stock"/>Stock
          <input type="text" name="price"/>Precio
         </div>
         <div class="enviar">
          <input type="submit" name="book-insert" value="Añadir">
         </div>
        </form>
       </section>';
    }

// Html row

    public function htmlRow() {
        return "
         <td>{$this->id}</td>
         <td>{$this->isbn}</td>
         <td>{$this->title}</td>
         <td>{$this->author}</td>
         <td>{$this->stock}</td>
         <td>{$this->price}</td>";
    }

// Generar Id

    private function newId() {
        $stm = $this->conect->query("SELECT id FROM book WHERE isbn = {$this->isbn}");
        if(!$stm) $id = 1;
        else {
            $result = $stm->fetch();
            if (!$result) {
                $stm = $this->conect->query("SELECT MAX(id) FROM book");
                $id = $stm->fetch()[0] + 1;
            }
            else $id = $result[0];
        }
        return $id;
    }

// Funciones DML

    public function insert() {
        $insert = $this->conect->prepare("INSERT INTO book VALUES (?, ?, ?, ?, ?, ?)");
        return $insert->execute([$this->id, $this->isbn, $this->title, $this->author, $this->stock, $this->price]);
    }

    public function update() {
        $update = $this->conect->exec("UPDATE book SET isbn = {$this->isbn}, title = '{$this->title}', author = '{$this->author}', stock = {$this->stock}, price = {$this->price} WHERE id = {$this->id}");
        return $update;
    }

    public function delete() {
        $delete = $this->conect->prepare("DELETE FROM book WHERE id = {$this->id}");
        return $delete->execute();
    }

    public static function selectBook($id) {
        $conection = new Conection();
        $stm = $conection->getConect()->query("SELECT isbn, title, author, stock, price FROM book WHERE id=$id");
        try {
            $resultado = $stm->fetch();
        }
        catch (Exception $e) {
            echo 'Libro no encontrado';
        }
        $book = new CBook($resultado);
        return $book;
    }
}
?>