<?php

namespace Figuras;

class CPentagono extends CFigura {
    private $side;

    public function __construct($side) {
        $this->side = $side;
        parent::__construct(CPentagono::calculateArea(), CPentagono::calculatePerimeter());
        $this->image = CPentagono::createImage();
    }

    private function calculateArea() {
        $this->perimeter = CPentagono::calculatePerimeter();
        $apotema = sqrt(pow($this->side, 2) - pow($this->side / 2, 2));
        return ($this->perimeter * $apotema) / 2;
    }

    private function calculatePerimeter() {
        return $this->side * 5;
    }

    private function createImage() {
        $image = imagecreatetruecolor(500, 500);

        $grey_dark = imagecolorallocate($image, 150, 150, 150);
        $red = imagecolorallocate($image, 255, 0, 0);
        $black = imagecolorallocate($image, 0, 0, 0);

        imagefill($image, 0, 0, $grey_dark);

        $center = 250;
        $side = $this->side * 15;
        
        imagefilledarc($image, $center, $center, $side, $side, 270, 342, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 342, 54, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 54, 126, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 126, 198, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 198, 270, $red, IMG_ARC_CHORD);

        imagefilledarc($image, $center, $center, $side, $side, 270, 342, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 342, 54, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 54, 126, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 126, 198, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center, $side, $side, 198, 270, $black, IMG_ARC_NOFILL | IMG_ARC_CHORD);

        return $image;
    }

    public static function inputForm() {
        return '<section>
        <form action="showFigure.php" method="post">
         <div class="select">
          <input type="number" name="side"/>Lado
          <input type="hidden" name="fig" value="Pentagono">
         </div>
         <div class="enviar">
          <input type="submit" name="btn-data" value="Seleccionar">
         </div>
        </form>
       </section>';
    }

    public function __toString() {
        return "<p>Lado: $this->side</p>".parent::__toString();
    }
}

?>