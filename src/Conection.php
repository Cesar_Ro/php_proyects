<?php
namespace Clases;

require '../vendor/autoload.php';
use PDO;
use PDOException;

class Conection {
    private $host;
    private $db;
    private $user;
    private $pass;
    private $dsn;
    protected $conect;

    public function __construct() {
        $config = json_decode(file_get_contents('config.json'), true);
        $this->host = $config['host'];
        $this->db = $config['db'];
        $this->user = $config['user'];
        $this->pass = $config['pass'];
        $this->dsn = "mysql:host={$this->host};dbname={$this->db};charset=utf8mb4";
        $this->conect = Conection::crearConect();
    }

    public function getConect() {
        return $this->conect;
    }

    private function crearConect() {
        try {
            $conect = new PDO($this->dsn, $this->user, $this->pass);
        }
        catch(PDOException $e) {
            if ($e->getCode() == 1049) {
                echo 'Database no encontrada, creando base de datos<br>';
                $conect = new PDO("mysql:host={$this->host}", $this->user, $this->pass);
                $query = $conect->exec("CREATE DATABASE IF NOT EXISTS {$this->db} COLLATE utf8_spanish_ci");

                if($query) {
                    $commands = file_get_contents("./{$this->db}.sql");
                    $tables = $conect->prepare($commands);
                    $tables->execute();
                    if(!$tables) echo "{$tables->errorInfo()}";
                }
                else echo 'Error al ejecutar archivo sql';

                $conect = new PDO($this->dsn, $this->user, $this->pass);
            }
        }
        return $conect;
    }
}

?>