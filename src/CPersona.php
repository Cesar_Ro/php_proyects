<?php

namespace Clases;

class CPersona {
    protected $name;
    protected $surname;

    public function __construct(string $name, string $surname) {
        $this->name = $name;
        $this->surname = $surname;
    }

    protected function getName() {
        return $this->name;
    }

    protected function getSurname() {
        return $this->surname;
    }

    public function __toString() {
        return "<p>$this->name $this->surname</p>";
    }
}
?>