<?php
namespace Cines;
require '../vendor/autoload.php';
use Clases\Conection;

class CCine extends Conection {
    private $id;
    private $name;
    private $address;
    private $postcode;
    private $city;

    public function __construct($array) {
        $this->id = $array[0];
        $this->name = $array[1];
        $this->address = $array[2];
        $this->postcode = $array[3];
        $this->city = $array[4];
        parent::__construct();
    }

// Metodos Get y Set
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getAddress() {
        return $this->address;
    }
    public function getPostcode() {
        return $this->postcode;
    }
    public function getCity() {
        return $this->city;
    }

// Formulario Input

// Funciones Auxiliares

    public function compareCine($cine) {
        return (
            $this->id == $cine->getId() &&
            $this->name == $cine->getName() &&
            $this->address == $cine->getAddress() &&
            $this->postcode == $cine->getPostcode() &&
            $this->city == $cine->getCity()
        );
    }

// Funciones DML
    public function insert() {
        $insert = $this->conect->prepare("INSERT INTO cine VALUES (?, ?, ?, ?, ?)");
        $result = $insert->execute([$this->id, $this->name, $this->address, $this->postcode, $this->city]);
        return [$insert->errorInfo(), $result];
    }

    public static function selectCine($id) {
        $conection = new Conection();
        $stm = $conection->getConect()->query("SELECT * FROM cine WHERE id=$id");
        try {
            $resultado = $stm->fetch();
        }
        catch (Exception $e) {
            echo 'Cine no encontrado';
        }
        $cine = new CCine($resultado);
        return $cine;
    }
}
?>