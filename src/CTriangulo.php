<?php

namespace Figuras;

class CTriangulo extends CFigura {
    private $side;
    private $altura;

    public function __construct($side) {
        $this->side = $side;
        $this->altura = CTriangulo::calculateAltura();

        parent::__construct(CTriangulo::calculateArea(), CTriangulo::calculatePerimeter());
        $this->image = CTriangulo::createImage();
    }

    private function calculateArea() {
        return $this->side * $this->altura / 2;
    }

    private function calculatePerimeter() {
        return $this->side * 3;
    }

    private function calculateAltura() {
        return $this->side * sin(deg2rad(60));
    }

    private function createImage() {
        $image = imagecreatetruecolor(500, 500);

        $grey_dark = imagecolorallocate($image, 150, 150, 150);
        $red = imagecolorallocate($image, 255, 0, 0);
        $black = imagecolorallocate($image, 0, 0, 0);

        imagefill($image, 0, 0, $grey_dark);

        $center = 250;
        $side = $this->side * 20;
        $altura = $this->altura * 5;
        
        imagefilledarc($image, $center, $center - $altura, $side, $side, 120, 60, $red, IMG_ARC_CHORD);
        imagefilledarc($image, $center, $center - $altura, $side, $side, 120, 60, $black, IMG_ARC_EDGED | IMG_ARC_NOFILL | IMG_ARC_CHORD);

        return $image;
    }

    public function getSide() {
        return $this->side;
    }

    public static function inputForm() {
        return '<section>
        <form action="showFigure.php" method="post">
         <div class="select">
          <input type="number" name="side"/>Lado
          <input type="hidden" name="fig" value="Triangulo">
         </div>
         <div class="enviar">
          <input type="submit" name="btn-data" value="Seleccionar">
         </div>
        </form>
       </section>';
    }

    public function __toString() {
        return "<p>Lado: $this->side</p>".parent::__toString();
    }
}

?>