<?php

namespace Figuras;

  abstract class CFigura {
    protected $area;
    protected $perimeter;
    protected $color;
    protected $image;

    protected function __construct($area, $perimeter) {
      $this->area = $area;
      $this->perimeter = $perimeter;
      $this->color = 'red';
    }

    public function getArea() {
      return $this->area;
    }

    public function getPerimeter() {
      return $this->perimeter;
    }

    public function getColor() {
      return $this->color;
    }


    public function __toString() {
      return "<p>Area: $this->area</p><p>Perimetro: $this->perimeter</p>";
    }

    public function showImage() {
      ob_start();
      imagejpeg($this->image, NULL, 100);
      $rawImageBytes = ob_get_clean();

      echo "<img src='data:image/jpeg;base64," . base64_encode( $rawImageBytes ) . "' />";
    }
  }
?>