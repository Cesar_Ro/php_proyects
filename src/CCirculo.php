<?php

namespace Figuras;

class CCirculo extends CFigura {
    private $radius;

    public function __construct($radius) {
        $this->radius = $radius;
        parent::__construct(CCirculo::calculateArea(), CCirculo::calculatePerimeter());
        $this->image = CCirculo::createImage();
    }

    private function calculateArea() {
        return $this->radius * $this->radius * pi();
    }

    private function calculatePerimeter() {
        return $this->radius * 2 * pi();
    }

    private function createImage() {
        $image = imagecreatetruecolor(500, 500);

        $grey_dark = imagecolorallocate($image, 150, 150, 150);
        $red = imagecolorallocate($image, 255, 0, 0);
        $black = imagecolorallocate($image, 0, 0, 0);

        imagefill($image, 0, 0, $grey_dark);

        $center = 250;
        $radius = $this->radius * 20;

        imagefilledellipse($image, $center, $center, $radius, $radius, $red);
        imageellipse($image, $center, $center, $radius, $radius, $black);

        return $image;
    }

    public function getRadius() {
        return $this->radius;
    }

    public static function inputForm() {
        return '<section>
        <form action="showFigure.php" method="post">
         <div class="select">
          <input type="number" name="radio"/>Radio
          <input type="hidden" name="fig" value="Circulo">
         </div>
         <div class="enviar">
          <input type="submit" name="btn-data" value="Seleccionar">
         </div>
        </form>
       </section>';
    }

    public function __toString() {
        return "<p>Radio: $this->radius</p>".parent::__toString();
    }
}

?>