<?php

namespace Clases;

class CHeader {
    public $text;
    public $background;
    public $color;

    public function __construct($word, $color_1, $color_2) {
        $this->text = $word;
        $this->background = $color_1;
        $this->color = $color_2;
    }

    public function __toString() {
        return "<header style=\"background-color:$this->background\"><p style=\"color:$this->color\">$this->text</p></header>";
    }

    public function printHeader() {

    }
}
?>