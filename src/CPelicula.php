<?php
namespace Cines;
require '../vendor/autoload.php';
use Clases\Conection;

class CPelicula extends Conection {
    private $uid;
    private $title;
    private $sale;
    private $image;
    private $calification;
    private $genre;
    private $time;
    private $summary;
    private $trailer;

    public function __construct($array) {
        $this->uid = $array[0];
        $this->title = $array[1];
        $this->sale = $array[2];
        $this->image = $array[3];
        $this->calification = $array[4];
        $this->genre = $array[5];
        $this->time = $array[6];
        $this->summary = $array[7];
        $this->trailer = $array[8];
        parent::__construct();
    }

// Metodos Get y Set
    public function getUid() {
        return $this->uid;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getSale() {
        return $this->sale;
    }
    public function getImage() {
        return $this->image;
    }
    public function getCalification() {
        return $this->calification;
    }
    public function getGenre() {
        return $this->genre;
    }
    public function getTime() {
        return $this->time;
    }
    public function getSummary() {
        return $this->summary;
    }
    public function getTrailer() {
        return $this->trailer;
    }

// Formulario Input

// Funciones Auxiliares

    public function comparePelicula($pelicula) {
        return (
            $this->uid == $pelicula->getUid() &&
            $this->title == $pelicula->getTitle() &&
            $this->sale == $pelicula->getSale() &&
            $this->image == $pelicula->getImage() &&
            $this->calification == $pelicula->getCalification() &&
            $this->genre == $pelicula->getGenre() &&
            $this->time == $pelicula->getTime() &&
            $this->summary == $pelicula->getSummary() &&
            $this->trailer == $pelicula->getTrailer() 
        );
    }

// Funciones DML
    public function insert() {
        $insert = $this->conect->prepare("INSERT INTO pelicula VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $result = $insert->execute([$this->uid, $this->title, $this->sale, $this->image, $this->calification, $this->genre, 
            $this->time, $this->summary, $this->trailer]);
        return [$insert->errorInfo(), $result];
    }

    public static function selectPelicula($uid) {
        $conection = new Conection();
        $stm = $conection->getConect()->query("SELECT * FROM pelicula WHERE uid=$uid");
        try {
            $resultado = $stm->fetch();
        }
        catch (Exception $e) {
            echo 'Pelicula no encontrado';
        }
        $pelicula = new CPelicula($resultado);
        return $pelicula;
    }
}
?>