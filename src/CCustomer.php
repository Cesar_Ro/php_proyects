<?php

namespace Libros;
require '../vendor/autoload.php';
use Clases\Conection;

class CCustomer extends Conection {
    private $id;
    private $firstName;
    private $surname;
    private $email;
    private $type;

    public function __construct($array) {
        $this->firstName = $array[0];
        $this->surname = $array[1];
        $this->email = $array[2];
        $this->type = $array[3];
        parent::__construct();
        $this->id = CCustomer::newId();
    }

// Métodos Get y Set

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getFirstname() {
        return $this->firstName;
    }
    public function getSurname() {
        return $this->surname;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getType() {
        return $this->type;
    }

// Html row

    public function htmlRow() {
        return "
         <td>{$this->id}</td>
         <td>{$this->firstName}</td>
         <td>{$this->surname}</td>
         <td>{$this->email}</td>
         <td>{$this->type}</td>";
}

// Formulario input

    public static function inputForm() {
        return '<section class="center">
        <form action="controller.php" method="post">
         <div class="select">
          <input type="text" name="firstname"/>Nombre
          <input type="text" name="surname"/>Apellido
          <input type="text" name="email"/>Email
          <select name="type">
            <option value="basic">Basic</option>
            <option value="premium">Premium</option>
          </select>
         </div>
         <div class="enviar">
          <input type="submit" name="customer-insert" value="Añadir">
         </div>
        </form>
       </section>';
    }

// Generar Id

    private function newId() {
        $stm = $this->conect->query("SELECT id FROM customer WHERE firstname = '{$this->firstName}' AND surname = '{$this->surname}'");
        if(!$stm) $id = 1;
        else {
            $result = $stm->fetch();
            if (!$result) {
                $stm = $this->conect->query("SELECT MAX(id) FROM customer");
                $id = $stm->fetch()[0] + 1;
            }
            else $id = $result[0];
        }
        return $id;
    }

// Funciones DML

    public function insert() {
        $insert = $this->conect->prepare("INSERT INTO customer VALUES (?, ?, ?, ?, ?)");
        return $insert->execute([$this->id, $this->firstName, $this->surname, $this->email, $this->type]);
    }

    public function update() {
        $update = $this->conect->prepare("UPDATE customer SET firstname = '{$this->firstName}', surname = '{$this->surname}', email = '{$this->email}', type = '{$this->type}' WHERE id = {$this->id}");
        return $update->execute();
    }

    public function delete() {
        $delete = $this->conect->prepare("DELETE FROM customer WHERE id = {$this->id}");
        return $delete->execute();
    }

    public static function selectCustomer($id) {
        $conection = new Conection();
        $stm = $conection->getConect()->query("SELECT firstname, surname, email, type FROM customer WHERE id = $id");
        try {
            $resultado = $stm->fetch();
        }
        catch (Exception $e) {
            echo 'Cliente no encontrado';
        }
        $customer = new CCustomer($resultado);
        return $customer;
    }
}
?>