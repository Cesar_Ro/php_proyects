<?php

namespace Figuras;

 class CCuadrilatero extends CFigura {
    private $sideA;
    private $sideB;

    public function __construct($sideA, $sideB) {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
        parent::__construct(CCuadrilatero::calculateArea(), CCuadrilatero::calculatePerimeter());
        $this->image = CCuadrilatero::createImage();
    }

    private function calculatePerimeter() {
        return $this->sideA * 2 + $this->sideB * 2;
    }

    private function calculateArea() {
        return $this->sideA * $this->sideB;
    }

    private function createImage() {
        $image = imagecreatetruecolor(500, 500);

        $grey_dark = imagecolorallocate($image, 150, 150, 150);
        $red = imagecolorallocate($image, 255, 0, 0);
        $black = imagecolorallocate($image, 0, 0, 0);

        imagefill($image, 0, 0, $grey_dark);

        $center = 250;
        $sideA = $this->sideA * 5;
        $sideB = $this->sideB * 5;

        imagefilledrectangle($image, $center - $sideA, $center - $sideB, $center + $sideA, $center + $sideB, $red);
        imagerectangle($image, $center - $sideA, $center - $sideB, $center + $sideA, $center + $sideB, $black);

        return $image;
    }

    public function getSideA() {
        return $this->sideA;
    }

    public function getSideB() {
        return $this->sideB;
    }

    public static function inputForm() {
        return '<section>
        <form action="showFigure.php" method="post">
         <div class="select">
          <input type="number" name="sideA"/>Lado A
          <input type="number" name="sideB"/>Lado B
          <input type="hidden" name="fig" value="Cuadrilatero">
         </div>
         <div class="enviar">
          <input type="submit" name="btn-data" value="Seleccionar">
         </div>
        </form>
       </section>';
    }

    public function __toString() {
        return "<p>Lado A: $this->sideA</p><p>Lado B: $this->sideB</p>".parent::__toString();
    }
 }
?>